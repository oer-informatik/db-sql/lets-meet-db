## Import der Exceldaten des "Let’s Meet"-Datenbankprojekts (LS Beispiellösung Teil 2)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/lets_meet-beispielloesung_import</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110207682728246503</span>

> **tl/dr;** _Dies ist der zweite Teil (Datenimport) einer Beispiellösung für die ["Let's Meet" Datenbank-Lernsituation](https://oer-informatik.de/lets-meet). Aus der Excel-Datei wird ein SQL-Importscript erstellt, das in einen neuen Docker-Container importiert wird. Dieser Artikel basiert auf Teil 1 der Beispiellösung [hier (Modellierung)](https://oer-informatik.de/lets_meet-beispielloesung_modellierung)._

Dieser Artikel ist Teil einer mehrteiligen Serie, in der

* [die eigentliche Aufgabestellung](https://oer-informatik.de/lets-meet) das Ausgangsszenario vorstellt und den Datenbank-Export als Tabelle verlinkt,
* im [ersten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) das Zielsystem modelliert wird (ER-Modell, Relationenmodell, physisches Modell / DDL),
* im [zweiten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_import) SQL-Befehle für den Import aus der Tabellenkalkulation erstellt werden und
* im [dritten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) die Ausgangsdaten über Zeichenkettenfunktionen, Datumsfunktionen, `JOINS` und Vereinigung von Abfragen schließlich Daten in das neue Zielsystem übernommen werden.

Allen Artikel enden mit einem SQL-Skript, mit dem Daten als Ausgangspunkt für die nächste Etappe genutzt werden können, falls es zu Problemen bei der Eigenentwicklung kam.

### Ausgangslage

Die eigentliche Aufgabestellung [der "Let's Meet" Datenbank-Lernsituation kann hier](https://oer-informatik.de/lets-meet) nachgelesen werden. Voraussetzung für den in diesem Teil beschriebenen Import ist eine fertig modellierte Datenbank, die als SQL-DDL-Befehl vorliegt wie es in dem [Vorgängerartikel (Teil 1: Modellierung)](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) beschrieben ist. Sämtliche Tabellen lassen sich beispielsweise über die fertigen SQL-Skripte für [PostgreSQL (hier)](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql) oder  [MariaDB (hier)](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql) erzeugen.


### Vorbereitung des DB-Containers 

Um den Datenbank-Container zu starten und direkt mit den richtigen Daten zu füllen benötigen wir ein leeres Verzeichnis, in dem lediglich das zuvor erstellte SQL-Skript (siehe Gitlab-Links oben) liegt. In der Bash und Powershell könnte das z.B. so aussehen:

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb">

Variante MariaDB (Anleitung zu MariaDB im Container [hier](https://oer-informatik.de/docker-install-mariadb)):

```powershell
cd ~/Documents
mkdir mariadbdocker
cd mariadbdocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql
```

Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen MariaDB-Container erzeugen und starten:

```powershell
docker run --name=letsmeetmaria -d -p 3307:3306 -e MARIADB_USER=dbuser -e MARIADB_PASSWORD=_hier%1auchGutesPWnu+2en -e MARIADB_ROOT_PASSWORD=_hier%1auchGutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d mariadb:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) MariaDB-Kommandozeile (`mariadb`) geöffnet werden können:

```powershell
docker exec -it letsmeetmaria mariadb -uroot -p
```

Nach Eingabe des Passworts sieht das Prompt etwa wie folgt aus:

```bash
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.9.3-MariaDB-1:10.9.3+maria~ubu2204 mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

Mit der folgenden Abfolge von SQL-Befehlen können die vorhandenen Datenbanken ausgegeben werden, die frisch angelegte `letsMeetMigrationScheme` ausgewählt werden und deren Tabellen angezeigt werden:

```sql
SHOW DATABASES;
USE letsMeetMigrationScheme;
SHOW TABLES;
```

Und dieser Befehl sollte zumindest eine leere Tabelle (`Empty set (0.000 sec)`) ausgeben:

```sql
SELECT * FROM letsMeetMigrationScheme.tblUrsprung;
```

Mit `exit` kann die SQL-Konsole wieder verlassen werden.

Weitere Details im Umgang mit MariaDB im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-mariadb).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

Variante MS-SQL (Anleitung zu MS-SQL-Server im Container [hier](https://oer-informatik.de/docker-install-mssql)):

Leider sind bei SQL-Server noch ein paar Handgriffe nötig, um einen befüllten Container zu erhalten. Neben den SQL-Skripten werden noch zwei Bash-Scripte und ein `Dockerfile` gebraucht. Alle Dateien stehen hier im Repo zur Verfügung (Befehle für PowerShell, in der Bash muss jeweils der `-OutFile "..."`-Teil weggelassen werden):

```powershell
cd ~/Documents
mkdir mssqlserverdocker
cd mssqlserverdocker
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/Dockerfile -OutFile "Dockerfile"
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/configure-db.sh -OutFile "configure-db.sh"
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/entrypoint.sh -OutFile "entrypoint.sh"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql -OutFile "letsmeet_1_struktur_mssqlserver.sql"
```

In diesem Verzeichnis muss jetzt ein `dockerimage` gebaut werden:

```powershell
PS> docker build -t mssql-custom .
```

Mit diesem Image erzeugen wir jetzt einen MS-SQL-Server-Container und starten diesen:

```powershell
PS> docker run --name=letsmeetmssql --hostname=letsmeetmssql -p 3309:1433 -e ACCEPT_EULA=Y -e MSSQL_SA_PASSWORD=_hier%1GutesPWnu+2en -d mssql-custom
```

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) SQL-Server-Kommandozeile (`sqlcmd`) für Nutzer `SA` mit dem eingegebenen Passwort geöffnet werden können:

```powershell
docker exec -it letsmeetmssql /opt/mssql-tools/bin/sqlcmd -S localhost -U SA
```

Und ein Befehl wie dieser hier sollte zumindest eine leere Tabelle ausgeben

```sql
SELECT * FROM letsmeetdb.letsMeetMigrationScheme.tblUrsprung;
GO
```

Weitere Details im Umgang mit SQL-Server im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-mssql).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre"  style="display:none">

```powershell
cd ~/Documents
mkdir psqldocker
cd psqldocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql
```
Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen postgreSQL-Container erzeugen und starten:

```powershell
docker run --name=letsmeetpsql -d -p 3312:5432 -e POSTGRES_PASSWORD=_hier%1GutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d postgres:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) PostgreSQL-Kommandozeile (`psql`) für Nutzer `postgres` mit Passwort `postgres` geöffnet werden können:

```powershell
docker exec -it letsmeetpsql psql -U postgres postgres
```

Die psql-Kommandozeile sieht etwa so aus:

```bash
psql (15.0 (Debian 15.0-1.pgdg110+1))
Type "help" for help.

postgres=# 
```

Und ein Befehl wie dieser hier sollte zumindest eine leere Tabelle ausgeben

```sql
SELECT * FROM letsMeetMigrationScheme.tblUrsprung;
```

Mit `\q` kann die Konsole wieder verlassen werden.

Weitere Details im Umgang mit PostgreSQL im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-postgres).

</span>



### Import in bestehende Struktur

Um die Ausgangsdaten in die SQL-Datenbank importieren zu können, müssen wir die einzelnen Werte aus [dieser Excel-Datei](http://oer-informatik.gitlab.io/db-sql/lets-meet-db/let_s-meet-db-dump.xlsx) importieren. Es gibt eine ganze Reihe Tools, die Excel-Tabellen in alle möglichen DBMS übernehmen - oft scheitern diese jedoch an Umlauten, Whitespaces, der Interpretation von Datumsfeldern, leeren Feldern usw.

Daher muss in jedem Fall am Ende des Imports überprüft werden, ob alle Werte übernommen wurden.

Um sicherzugehen, was importiert wird, kann das SQL-INSERT-Statement per Excel-Formel generiert werden. Excel Formeln können den Inhalt anderer Zellen wiedergeben. Gibt man in der Excel-Zelle `I2` beispielsweise die Formel `=A2` ein, so wird der Inhalt der Zelle `A2` in `I2` angezeigt. Zeichenketten werden in Excel mit `&` verknüpft, zudem können in Anführungszeichen weitere Zeichenketten eingegeben werden: `="1: "&A2&", 2:"&B2` fügt den Inhalt zweier Zellen aneinander.

Ziel soll es sein, aus einer Excel-Zeile ein SQL-INSERT-Statement zu erstellen nach dem Muster:

```sql
INSERT INTO tblNamen (vorname, nachname) VALUES("Max","Mustermann");
```

Besonderes Augenmerk sollte hierbei auf den Anführungszeichen liegen. Ich hatte oben doppelte Anführungszeichen zur Trennung der Werte verwendet (`"Max"`). In Excel trennen diese jedoch die Zeichenketten. Wir nutzen also für das SQL-Statement neben den doppelten Anführungszeichen zur Abtrennung der Excel-Zeichenketten zusätzlich die einfachen Anführungszeichen zur Abtrennung der Werte des Import-Befehls:

```excel
="INSERT INTO tblNamen (vorname, nachname) VALUES('"&A2&"','"&B2&"');"
```

Natürlich muss vorher überprüft werden, ob in den eingefügten Werten Anführungszeichen vorhanden sind, da sonst das SQL-Statement die Zeichenketten falsch interpretiert.


Damit ist die Grundstruktur der Formel gelegt, um aus einer Excel-Tabelle SQL-Befehle zu generieren. Eine Formel nach diesem Muster müsste also in eine freie Spalte (z.B. `I2` in der Beispieltabelle) erstellt werden und dort die Werte und Attributnamen aller Spalten enthalten. Diese Formel müsste dann in alle Zeilen mit Werte kopiert werden. Die so erstellten SQL-Befehle können wiederum kopiert und in einem SQL-Frontend ausgeführt werden.

Prinzipiell gibt es die Möglichkeit, aus jeder Zeile einen einzelnen SQL-Befehl zu erstellen, etwa so:

```sql
INSERT INTO tblNamen (vorname, nachname) VALUES("Max","Mustermann");
INSERT INTO tblNamen (vorname, nachname) VALUES("Martina","Mustermann");
```

Oder alle Werte in einem gemeinsamen Statement als Einzeiler zusammenzufassen:

```sql
INSERT INTO tblNamen VALUES("Max","Mustermann"),("Martina","Mustermann");
```

Während die erste Variante den Vorteil hat, dass wir bei auftretenden Fehlern genau sehen, welches SQL-Statement einen Fehler verursacht, haben wir andererseits das Problem, dass ggf. nicht alle Befehle ausgeführt werden, ohne dass wir das merken.

Ich erstelle daher mehrzeilige SQL-Befehle zum Debuggen, den Import selbst führe ich aber mit einem Einzeiler durch, um sicherzugehen, dass alle Zeilen importiert werden (oder im Fehlerfall eben keine, was ich schnell merke).


Meine Mehrzeiler-Formel für die erste Wertzeile (Excel-Zeile 2), die ich in Zelle `J2` eingefügt und nach unten kopiert habe, lautet:

```excel
="INSERT INTO letsMeetMigrationScheme.tblUrsprung (namen, adresse, telefon, hobbytext, email, geschlecht, interesse, geburtstag) VALUES('"&A2&"', '"&B2&"', '"&C2&"', '"&D2&"', '"&E2&"', '"&F2&"', '"&G2&"', '"&H2&"');"
```


Für den Einzeiler-Befehl benötige ich nur die Klammer mit dem Werten aus der obigen Formel, wobei die einzelnen Klammern mit Kommata getrennt werden: 

```excel
="('"&A2&"', '"&B2&"', '"&C2&"', '"&D2&"', '"&E2&"', '"&F2&"', '"&G2&"', '"&H2&"'),"
```

Diese Wert-Zeilen werden dann später zu einem SQL-Statement ergänzt:

```sql
INSERT INTO letsMeetMigrationScheme.tblUrsprung (namen, adresse, telefon, hobbytext, email, geschlecht, interesse, geburtstag) VALUES
/*HIER DAS ERGEBNIS DER FORMEL EINFÜGEN UND DAS LETZTE KOMMA LÖSCHEN*/
;
```

Wichtig ist, dass das letzte Komma gelöscht (bzw. durch einen Semikolon ersetzt) werden muss, da sonst der SQL-Befehl nicht durchläuft.


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb">

Keine Besonderheiten bei MariaDB - das Skript müsste so durchlaufen.

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

Eine Besonderheit bei MS SQL Server ist, dass hier einzelne Abfragen, die mehr als 1000 Zeilen einfügen (wie unsere Einzeilervariante), nicht ohne weiteres ausgeführt werden können. Es muss also auf Alternativen zum Import großer Datenmengen ausgewichen werden. 

- Eine Variante ist der oben genannte Mehrzeiler, bei dem jeder Befehl genau einen Datensatz einfügt. Diese Lösung findet sich [hier als SQL-Skript](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_mehrzeiler_mssqlserver.sql)

- Eine zweite Variante ist die Umwandlung des INSERT-Scripts: Werden die Werte nicht direkt als Liste angegeben (`VALUES...`), sondern über einen Subselect (`SELECT * FROM (VALUES ...`), dann ist der Import möglich. Wir müssen also umformulieren, statt...

```sql
INSERT INTO tabelle (attr1, attr2) 
VALUES 
 (wert1, wert2);
 (wert3, wert4);
```

... muss es heißen:

```sql
INSERT INTO tabelle (attr1, attr2) 
  SELECT * FROM (
    VALUES 
     (wert1, wert2), 
     (wert3, wert4)
  ),
```

Im konkreten Beispiel sieht das so aus:

```sql
INSERT INTO letsMeetMigrationScheme.tblUrsprung (namen, adresse, telefon, hobbytext, email, geschlecht, interesse, geburtstag)
SELECT * FROM (
  VALUES
  ('Forster, Martin', 'Minslebener Str. 0, 46286, Dorsten', '02372 8020', 'Fremdsprachenkenntnisse erweitern %78%; Im Wasser waten %80%; Schwierige Probleme klären %61%; Morgens Früh aufstehen %17%; ', 'martin.forster@web.ork', 'm', 'w', '07.03.1959'),
  ('Elina, Tsanaklidou', 'Gartenweg 13, 69126, Heidelberg', '06221 / 98689', 'Jemanden massieren %57%; Mir die Probleme von anderen anhören %21%; Abends seinem Partner Ereignisse des Tages erzählen %53%; Für einen guten Zweck spenden %25%; ', 'tsanaklidou.elina@1mal1.te', 'w', 'm', '28.02.1958')
) AS temp (namen, adresse, telefon, hobbytext, email, geschlecht, interesse, geburtstag);
```

Diese Lösung findet sich [hier als SQL-Skript](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

Keine Besonderheiten bei PostgreSQL - das Skript müsste so durchlaufen.

</span>

### Testen des Imports

Um sicherzustellen, dass alle Daten importiert wurden, sollten wir zumindest rudimentäre Plausibilitätskontrollen vornehmen. Wurden alle Zeilen importiert? Wurden alle Zeichen importiert? Wir zählen in der Excel-Tabelle nach.

Spaltenweise lassen sich die Zeichen zählen z.B. über die Matrixfunktion (in Excel Formel eingeben, dann `Strg`-`Shift`-`Return` drücken):

```excel
=SUMME(LÄNGE(Tabelle1!A2:A1577))
```

(Für die anderen Spalten entsprechend)

Es ergeben sich folgende Testfälle, die sich leider durch unterschiedliche Abfragen und unterschiedliche Zählweise der DBMS in den Ergebnissen unterscheiden.

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb">


|Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte | Erwartetes<br/>Resultat<br/>gemäß<br/>Spezifikation | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
--- | --- | --- |--- | --- | --- |--- | ---
|Import-1 | Alle Zeilen importiert? |  |`SELECT COUNT(*) from letsMeetMigrationScheme.tblUrsprung;` | 1576 | - | ||
|Import-2 | Alle Namen importiert?|  |`SELECT SUM(CHAR_LENGTH(namen)) from letsMeetMigrationScheme.tblUrsprung;`| 24315 | - |||
|Import-3 | Alle Adressen importiert?|  |`SELECT SUM(CHAR_LENGTH(adresse)) from letsMeetMigrationScheme.tblUrsprung;`| 52731 | - |||
|Import-4 | Alle Telefonnummern importiert?| |`SELECT SUM(CHAR_LENGTH(telefon)) from letsMeetMigrationScheme.tblUrsprung;`| 20021 | - |||
|Import-5 | Alle Hobbys importiert? |  |`SELECT SUM(CHAR_LENGTH(hobbytext)) from letsMeetMigrationScheme.tblUrsprung;` | 174464 | - |||
|Import-6 | Alle E-Mail-Adressen importiert? |  |`SELECT SUM(CHAR_LENGTH(email)) from letsMeetMigrationScheme.tblUrsprung;` | 40305 | - |||
|Import-7 | Alle Geschlechter importiert? |  |`SELECT SUM(CHAR_LENGTH(geschlecht)) from letsMeetMigrationScheme.tblUrsprung;` | 1614 | - |||
|Import-8 | Alle Interessen importiert? |  |`SELECT SUM(CHAR_LENGTH(interesse)) from letsMeetMigrationScheme.tblUrsprung;` | 1609 | - |||
|Import-9 | Alle Geburtstage importiert? |  |`SELECT SUM(CHAR_LENGTH(geburtstag)) from letsMeetMigrationScheme.tblUrsprung;` | 15760 | - |||


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">


|Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte | Erwartetes<br/>Resultat<br/>gemäß<br/>Spezifikation | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
--- | --- | --- |--- | --- | --- |--- | ---
|Import-1 | Alle Zeilen importiert? |  |`SELECT COUNT(*) from letsMeetMigrationScheme.tblUrsprung;` | 1576 | - | ||
|Import-2 | Alle Namen importiert?|  |`SELECT SUM(LENGTH(namen)) from letsMeetMigrationScheme.tblUrsprung;`| 24321<br/>MariaDB: 24315 | - |||
|Import-3 | Alle Adressen importiert?|  |`SELECT SUM(LENGTH(adresse)) from letsMeetMigrationScheme.tblUrsprung;`| 52708 | - |||
|Import-4 | Alle Telefonnummern importiert?| |`SELECT SUM(LENGTH(telefon)) from letsMeetMigrationScheme.tblUrsprung;`| 20021 | - |||
|Import-5 | Alle Hobbys importiert? |  |`SELECT SUM(LENGTH(hobbytext)) from letsMeetMigrationScheme.tblUrsprung;` | 176030 | - |||
|Import-6 | Alle E-Mail-Adressen importiert? |  |`SELECT SUM(LENGTH(email)) from letsMeetMigrationScheme.tblUrsprung;` | 40305 | - |||
|Import-7 | Alle Geschlechter importiert? |  |`SELECT SUM(LENGTH(geschlecht)) from letsMeetMigrationScheme.tblUrsprung;` | 1614 | - |||
|Import-8 | Alle Interessen importiert? |  |`SELECT SUM(LENGTH(interesse)) from letsMeetMigrationScheme.tblUrsprung;` | 1609 | - |||
|Import-9 | Alle Geburtstage importiert? |  |`SELECT SUM(LENGTH(geburtstag)) from letsMeetMigrationScheme.tblUrsprung;` | 15760 | - |||

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">


|Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte | Erwartetes<br/>Resultat<br/>gemäß<br/>Spezifikation | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
--- | --- | --- |--- | --- | --- |--- | ---
|Import-1 | Alle Zeilen importiert? |  |`SELECT COUNT(*) from letsMeetMigrationScheme.tblUrsprung;` | 1576 | - | ||
|Import-2 | Alle Namen importiert?|  |`SELECT SUM(LENGTH(namen)) from letsMeetMigrationScheme.tblUrsprung;`| 24321<br/>MariaDB: 24315 | - |||
|Import-3 | Alle Adressen importiert?|  |`SELECT SUM(LENGTH(adresse)) from letsMeetMigrationScheme.tblUrsprung;`| 52708 | - |||
|Import-4 | Alle Telefonnummern importiert?| |`SELECT SUM(LENGTH(telefon)) from letsMeetMigrationScheme.tblUrsprung;`| 20021 | - |||
|Import-5 | Alle Hobbys importiert? |  |`SELECT SUM(LENGTH(hobbytext)) from letsMeetMigrationScheme.tblUrsprung;` | 176030 | - |||
|Import-6 | Alle E-Mail-Adressen importiert? |  |`SELECT SUM(LENGTH(email)) from letsMeetMigrationScheme.tblUrsprung;` | 40305 | - |||
|Import-7 | Alle Geschlechter importiert? |  |`SELECT SUM(LENGTH(geschlecht)) from letsMeetMigrationScheme.tblUrsprung;` | 1614 | - |||
|Import-8 | Alle Interessen importiert? |  |`SELECT SUM(LENGTH(interesse)) from letsMeetMigrationScheme.tblUrsprung;` | 1609 | - |||
|Import-9 | Alle Geburtstage importiert? |  |`SELECT SUM(LENGTH(geburtstag)) from letsMeetMigrationScheme.tblUrsprung;` | 15760 | - |||

</span>


### Fazit des Imports

Damit wären die Daten der Ausgangstabelle in `tblUrsprung` importiert. Ein fertiges SQL-Skript für 

- MariaDB findet sich [hier als Mehrzeiler](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_mehrzeiler_mariadb.sql) bzw. [hier als Einzeiler](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql).

- MS SQL Server findet sich [hier als Mehrzeiler](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_mehrzeiler_mssqlserver.sql) bzw. [hier als Einzeiler](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql).

- PostgreSQL findet sich [hier als Mehrzeiler](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_mehrzeiler_postgre.sql) bzw. [hier als Einzeiler](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/blob/raw/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql) 


Natürlich muss vorher die Struktur angelegt werden, z.B. mit dem entsprechenden Skript aus Teil 1 der Beispiellösung (siehe oben oder in Teil 1).

Es fehlt noch die eigentliche Normalisierung im Bestand - das wird in [einem weiteren Artikel](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) beschrieben. 

### Alle Artikel der Beispiellösung

Dieser Artikel ist Teil einer mehrteiligen Serie, in der

- [die eigentliche Aufgabestellung](https://oer-informatik.de/lets-meet) das Ausgangsszenario vorstellt und den Datenbank-Export als Tabelle verlinkt,

- im [ersten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) das Zielsystem modelliert wird (ER-Modell, Relationenmodell, physisches Modell / DDL),

- im [zweiten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_import) SQL-Befehle für den Import aus der Tabellenkalkulation erstellt werden und

- im [dritten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) die Ausgangsdaten über Zeichenkettenfunktionen, Datumsfunktionen, JOINS und Vereinigung von Abfragen schließlich Daten in das neue Zielsystem übernommen werden.

### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)

