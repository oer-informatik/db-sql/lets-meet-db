## Normalisierung per DML-Queries eines bestehenden Datenbankinhalts (LS Beispiellösung Teil 3)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/lets_meet-beispielloesung_normalisierung</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110207682728246503</span>


> **tl/dr;** _Dies ist der dritte Teil (Normalisierung) einer Beispiellösung für die ["Let's Meet" Datenbank-Lernsituation](https://oer-informatik.de/lets-meet). Die Daten sollen nun über Zeichenkettenoperationen, JOIN, UNION und Subselects im Bestand normalisiert und in neue Tabellen übernommen werden._

Dieser Artikel ist Teil einer mehrteiligen Serie, in der

-  [die eigentliche Aufgabestellung](https://oer-informatik.de/lets-meet) das Ausgangsszenario vorstellt und den Datenbank-Export als Tabelle verlinkt,

- im [ersten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) das Zielsystem modelliert wird (ER-Modell, Relationenmodell, physisches Modell / DDL),

- im [zweiten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_import) SQL-Befehle für den Import aus der Tabellenkalkulation erstellt werden und

- im [dritten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) die Ausgangsdaten über Zeichenkettenfunktionen, Datumsfunktionen, JOINS und Vereinigung von Abfragen schließlich Daten in das neue Zielsystem übernommen werden.

Allen Artikel enden mit einem SQL-Skript, mit dem Daten als Ausgangspunkt für die nächste Etappe genutzt werden können, falls es zu Problemen bei der Eigenentwicklung kam.

### Ausgangslage

Die eigentliche Aufgabestellung [der "Let's Meet" Datenbank-Lernsituation kann hier](https://oer-informatik.de/lets-meet) nachgelesen werden. Voraussetzung für die in diesem Teil beschriebene Normalisierung ist eine fertig modellierte und importierte Datenbank (siehe [Teil 1 ](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) und [Teil 2](https://oer-informatik.de/lets_meet-beispielloesung_import)).  

### Schnelleinstieg ohne Vorkenntnisse: Vorbereitung des DB-Containers 

Die Grundlagen der beiden vorigen Artikel münden in einer Datenbank, die sich mit SQL-Skripten erzeugen und füllen lässt. 
Wer so weit ist, der muss nur seinen Container wieder starten (sofern dieser nicht noch läuft) und die folgenden Zeilen überspringen bis zum Absatz "Normalisierung vorbereiten in tblUrsprung".

Bei wem dies nicht geglückt ist, der kann mit folgenden Schritten einen befüllten Datenbank-Container erzeugen, der direkt startklar ist. Hierfür wird nur ein leeres Verzeichnis und Docker benötigt. In das Verzeichnis müssen alle SQL-Skripte mit benötigten Befehlen kopiert werden, die zuvor erstellt (oder heruntergeladen) wurden. Die Skripte werden in alphabetischer Reihenfolge ausgeführt. In der Bash und Powershell könnte das z.B. so aussehen:


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

Variante MariaDB (Anleitung zu MariaDB im Container [hier](https://oer-informatik.de/docker-install-mariadb)).
Die folgenden Befehle funktionieren in der PowerShell, für die Linux-Bash muss der `-OutFile "..."`-Teil weggelassen werden.

```powershell
cd ~/Documents
mkdir mariadbdocker
cd mariadbdocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql -OutFile "letsmeet_1_struktur_mariadb.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql  -OutFile "letsmeet_2_import_einzeiler_mariadb.sql"
```

Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen MariaDB-Container erzeugen und starten:

```powershell
docker run --name=letsmeetmaria -d -p 3307:3306 -e MARIADB_USER=dbuser -e MARIADB_PASSWORD=_hier%1auchGutesPWnu+2en -e MARIADB_ROOT_PASSWORD=_hier%1auchGutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d mariadb:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) MariaDB-Kommandozeile (`mariadb`) geöffnet werden können:

```powershell
docker exec -it letsmeetmaria mariadb -uroot -p
```

Nach Eingabe des Passworts sieht das Prompt etwa wie folgt aus:

```bash
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.9.3-MariaDB-1:10.9.3+maria~ubu2204 mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

Mit der folgenden Abfolge von SQL-Befehlen können die vorhandenen Datenbanken ausgegeben werden, die frisch angelegte `letsMeetMigrationScheme` ausgewählt werden und deren Tabellen angezeigt werden:

```sql
SHOW DATABASES;
USE letsMeetMigrationScheme;
SHOW TABLES;
```

Und dieser Befehl sollte jetzt eine gefüllte Tabelle ausgeben:

```sql
SELECT * FROM letsMeetMigrationScheme.tblUrsprung;
```

```
1576 rows in set (0.007 sec)
```

Mit `exit` kann die SQL-Konsole wieder verlassen werden.

Weitere Details im Umgang mit MariaDB im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-mariadb).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

Variante MS-SQL (Anleitung zu MS-SQL-Server im Container [hier](https://oer-informatik.de/docker-install-mssql)):

Leider sind bei SQL-Server noch ein paar Handgriffe nötig, um einen befüllten Container zu erhalten. Neben den SQL-Skripten werden noch zwei Bash-Scripte und ein `Dockerfile` gebraucht. Alle Dateien stehen hier im Repo zur Verfügung (Befehle für PowerShell, in der Bash muss jeweils der `-OutFile "..."`-Teil weggelassen werden):

```powershell
cd ~/Documents
mkdir mssqlserverdocker
cd mssqlserverdocker
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/Dockerfile -OutFile "Dockerfile"
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/configure-db.sh -OutFile "configure-db.sh"
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/entrypoint.sh -OutFile "entrypoint.sh"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql -OutFile "letsmeet_1_struktur_mssqlserver.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql -OutFile "letsmeet_2_import_einzeiler_mssqlserver.sql"
```

In diesem Verzeichnis muss jetzt ein `dockerimage` gebaut werden:

```powershell
PS> docker build -t mssql-custom .
```

Mit diesem Image erzeugen wir jetzt einen MS-SQL-Server-Container und starten diesen:

```powershell
PS> docker run --name=letsmeetmssql --hostname=letsmeetmssql -p 3309:1433 -e ACCEPT_EULA=Y -e MSSQL_SA_PASSWORD=_hier%1GutesPWnu+2en -d mssql-custom
```

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) SQL-Server-Kommandozeile (`sqlcmd`) für Nutzer `SA` mit dem eingegebenen Passwort geöffnet werden können:

```powershell
docker exec -it letsmeetmssql /opt/mssql-tools/bin/sqlcmd -S localhost -U SA
```

Und ein Befehl wie dieser hier sollte den Inhalt der Ausgangsdaten ausgeben:

```sql
SELECT * FROM letsmeetdb.letsMeetMigrationScheme.tblUrsprung;
GO
```

Weitere Details im Umgang mit SQL-Server im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-mssql).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

Die folgenden Befehle sind für die PowerShell. In der Linux-Bash und unter OSX muss der `-OutFile "..."`-Teil weggelassen werden! Es wird im Home-Verzeichnis unter Documents ein neuer Ordner angelegt (also `~/Documents/psqldocker`), die Namen können natürlich individuell vergeben werden!

```powershell
cd ~/Documents
mkdir psqldocker
cd psqldocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql -OutFile "letsmeet_1_struktur_ddl_postgre.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_mehrzeiler_postgre.sql  -OutFile "letsmeet_2_import_einzeiler_postgre.sql" 
```

Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen postgreSQL-Container erzeugen und starten:

```powershell
docker run --name=letsmeetpsql -d -p 3312:5432 -e POSTGRES_PASSWORD=_hier%1GutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d postgres:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Das dauert diesmal etwas, weil 1576 Zeilen importiert werden müssen. Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) PostgreSQL-Kommandozeile (`psql`) für Nutzer `postgres` mit Passwort `postgres` geöffnet werden können:

```powershell
docker exec -it letsmeetpsql psql -U postgres postgres
```

Die psql-Kommandozeile sieht etwa so aus:

```bash
psql (15.0 (Debian 15.0-1.pgdg110+1))
Type "help" for help.

postgres=# 
```

Und ein Befehl wie dieser hier sollte im Idealfall die Anzahl der Zeilen (1576) ausgeben:

```sql
SELECT COUNT(*) FROM letsMeetMigrationScheme.tblUrsprung;
```

Mit `\q` kann die Konsole wieder verlassen werden.

Weitere Details im Umgang mit PostgreSQL im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-postgres).

</span>

### Normalisierung vorbereiten in tblUrsprung

Die Strategie der Normalisierung und Anpassung der Daten im Bestand ist immer die gleiche:

1. Zunächst werden die Daten lesend so aufbereitet, wie wir sie später benötigen (`SELECT`-Statments, ohne Änderung des Datenbestands)

2. Die Struktur der Datenbank wird so angepasst, wie wir sie benötigen, um die aufbereiteten Daten zu speichern (z.B. neue Spalten per `ALTER TABLE` anfügen)

3. Basierend auf dem `SELECT`-Statement aus (1) wird ein SQL-DML-Statement (DML = **D**ata **M**anipulation **L**anguage) erstellt, dass die Daten ändert / einfügt (`UPDATE` oder `INSERT`)

4. Mit Hilfe von `SELECT`-Statements wird im Anschluss geprüft, ob die Abfrage wie gewünscht funktioniert hat.

Wir führen diese Anpassungen in zwei Schritten durch: zunächst bereiten wir alle Daten in `tblUrsprung` so auf, wie wir sie benötigen. In einem zweiten Schritt übernehmen wir diese Daten dann in die jeweiligen Zieltabellen. In den folgenden Absätzen geht es zunächst um die Anpassungen in `tblUrsprung`.

#### Das Namensfeld in Vorname/Nachname aufteilen

Als Erstes kümmern wir uns um Vorname / Nachname. Die Techniken und Befehle, die wir hier verwenden, werden uns bei einer Vielzahl an Anpassungen helfen. Es geht wie angekündigt im Dreischritt voran: (1) Daten zunächst ohne Datenänderung umformen/filtern (`SELECT`), (2) Datenbank anpassen (`ALTER TABLE`), (3) Daten ändern (`INSERT` / `UPDATE`):

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1.  Lesend die Daten aufbereiten

Um die bestehenden Daten aufzubereiten, müssen wir Zeichenkettenfunktionen bemühen. Eine erste Übersicht für verschiedene DBMS und Links, um die passenden Funktionen aus den Dokumentationen zu finden, ist [hier](https://oer-informatik.de/sql-dml-singlerowfunctions) beschrieben. 

Die wichtigsten Zeichenkettenoperationen von MariaDB, die uns hier helfen, sind folgende:


- Teilstrings erzeugen (rechts, links, oder positionsabhängig): `SUBSTRING(...)`

- Zeichenkettenlängen bestimmen: `CHAR_LENGTH()`

- Position von Zeichen in Zeichenketten: `LOCATE(...)`

- Zeichen von Whitespaces entfernen: `TRIM(...)`

Für unsere jeweilige Aufgabe müssen wir jetzt nur noch einen passenden Separator finden, mithilfe dessen wir die Zeichenkette zerteilen können.

![Die bisherige Spaltenamen enthält Vorname und Nachname](beispielloesung/images/substring.png)

Im ersten Beispiel ist der Separator ein Komma (plus Leerzeichen), schematisch benötigen wir also etwa folgende Befehle für die Komponenten:

![Am Beispiel einer Zeile skizziert die pos() und substring() Funktion](beispielloesung/images/substring2.png)

MariaDB-Beispiel:

```sql
SELECT
 namen, 
 TRIM(SUBSTRING( namen, 1, LOCATE(', ', namen)-1)) AS lastname,
 TRIM(SUBSTRING( namen, LOCATE(', ', namen )+2 )) AS firstame,
 CHAR_LENGTH ( namen ) AS Gesamtlaenge,
 LOCATE(', ', namen ) AS Kommaposition,
 CHAR_LENGTH ( TRIM(SUBSTRING( namen, 1, LOCATE(', ', namen)-1)) ) AS LaengeNachname,
 CHAR_LENGTH ( TRIM(SUBSTRING( namen, LOCATE(', ', namen )+2 ))) AS LaengeVorname
 FROM letsMeetMigrationScheme.tblUrsprung;
```


2.  Die nötigen neuen Spalten erzeugen

Die Ergebnisse dieser Abfrage speichern wir zunächst in einem neuen Attribut (eine neue Spalte) in `tblUrsprung`. Es vereinfacht das Verfahren erheblich, wenn wir die Werte nicht direkt in die eigentliche Zieltabelle `tblUser` übernehmen, sondern unser Gesamtproblem atomisieren und kleinschrittig lösen. Wir benötigen also neue Attribute und müssen unsere Tabelle anpassen:

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD firstname CHAR(32);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD lastname CHAR(32);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

Es folgt der eigentliche datenverändernde Schritt: Wir übernehmen die Projektion aus den `SELECT`-Befehlen, die wir oben erstellt hatten, und weisen diese Werte den neuen Attributen zu. Durch diese Aufteilung können wir zunächst gefahrlos die Ergebnisse der Projektion begutachten, und erst, wenn wir keine Probleme mehr entdecken, die Daten schreiben.

Aus der Projektion `TRIM(SUBSTRING( namen, LOCATE(', ', namen )+2 )) AS firstame` wird so die Zuweisung ` firstname =TRIM(SUBSTRING( namen, LOCATE(', ', namen )+2 ))`. In die Syntax eines `UPDATE`-Befehls gegossen wird daraus:


```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 lastname =TRIM(SUBSTRING( namen, 1, LOCATE(', ', namen)-1)),
 firstname =TRIM(SUBSTRING( namen, LOCATE(', ', namen )+2 ));
```

4. Prüfen per `SELECT`-Befehl

Zur Überprüfung sollten wir uns mindestens die alten und neuen Attribute einmal anschauen und sortieren, ob es leere Felder gibt, um diese gesondert zu überprüfen. Hat alles geklappt?

```sql
SELECT namen, firstname, lastname  FROM letsMeetMigrationScheme.tblUrsprung ORDER BY lastname ASC;
```

Ein einfacher Weg, die Vollständigkeit eines Imports zu überprüfen, wenn man die Daten kennt, ist das Zählen der Zeichen:

```sql
SELECT SUM(CHAR_LENGTH(namen)), SUM(CHAR_LENGTH(firstname)),  SUM(CHAR_LENGTH(lastname))  FROM letsMeetMigrationScheme.tblUrsprung;
```

|SUM(CHAR_LENGTH(namen))|SUM(CHAR_LENGTH(firstname))|SUM(CHAR_LENGTH(lastname))|
|---|---|---|
|24735|10307|11208|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">



1.  Lesend die Daten aufbereiten

Um die bestehenden Daten aufzubereiten, müssen wir Zeichenkettenfunktionen bemühen. Eine erste Übersicht für verschiedene DBMS und Links, um die passenden Funktionen aus den Dokumentationen zu finden, ist [hier](https://oer-informatik.de/sql-dml-singlerowfunctions) beschrieben. 

Die wichtigsten Zeichenkettenoperationen von MS SQL Server, die uns hier helfen, sind folgende:


- Teilstrings erzeugen (rechts, links, oder positionsabhängig): `SUBSTRING(...)`, `LEFT (str, pos)`, `RIGHT (str, pos)`

- Zeichenkettenlängen bestimmen: `LEN()`

- Position von Zeichen in Zeichenketten: `PATINDEX (...)`

- Zeichen von Whitespaces entfernen: `TRIM(...)`

Für unsere jeweilige Aufgabe müssen wir jetzt nur noch einen passenden Separator finden, mithilfe dessen wir die Zeichenkette zerteilen können.

![Die bisherige Spaltenamen enthält Vorname und Nachname](beispielloesung/images/substring.png)

Im ersten Beispiel ist der Separator ein Komma (plus Leerzeichen), schematisch benötigen wir also etwa folgende Befehle für die Komponenten:

![Am Beispiel einer Zeile skizziert die pos() und substring() Funktion](beispielloesung/images/substring2.png)

SQL Server-Beispiel:

```sql
SELECT
 namen, 
 TRIM(LEFT( namen, PATINDEX('%, %', namen)-1)) AS lastname,
 TRIM(RIGHT(RTRIM(namen), LEN( namen )-PATINDEX('%, %', namen ))) AS firstame,
 LEN( namen ) AS Gesamtlaenge,
 PATINDEX('%, %', namen ) AS Kommaposition,
 LEN( TRIM(LEFT( namen, PATINDEX('%, %', namen)-1)) ) AS LaengeNachname,
 LEN( TRIM(RIGHT( RTRIM(namen), LEN( namen )-PATINDEX('%, %', namen )))) AS LaengeVorname
 FROM letsMeetMigrationScheme.tblUrsprung;
```


2.  Die nötigen neuen Spalten erzeugen

Die Ergebnisse dieser Abfrage speichern wir zunächst in einem neuen Attribut (eine neue Spalte) in `tblUrsprung`. Es vereinfacht das Verfahren erheblich, wenn wir die Werte nicht direkt in die eigentliche Zieltabelle `tblUser` übernehmen, sondern unser Gesamtproblem atomisieren und kleinschrittig lösen. Wir benötigen also neue Attribute und müssen unsere Tabelle anpassen:

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD firstname CHAR(32);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD lastname CHAR(32);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

Es folgt der eigentliche datenverändernde Schritt: Wir übernehmen die Projektion aus den `SELECT`-Befehlen, die wir oben erstellt hatten, und weisen diese Werte den neuen Attributen zu. Durch diese Aufteilung können wir zunächst gefahrlos die Ergebnisse der Projektion begutachten, und erst, wenn wir keine Probleme mehr entdecken, die Daten schreiben.

Aus der Projektion `TRIM(RIGHT(RTRIM(namen), LEN( namen )-PATINDEX('%, %', namen ))) AS firstame` wird so die Zuweisung ` firstname =TRIM(RIGHT(RTRIM(namen), LEN( namen )-PATINDEX('%, %', namen )))`. In die Syntax eines `UPDATE`-Befehls gegossen wird daraus:


```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 lastname =TRIM(LEFT( namen, PATINDEX('%, %', namen)-1)),
 firstname =TRIM(RIGHT(RTRIM(namen), LEN( namen )-PATINDEX('%, %', namen )));
```

4. Prüfen per `SELECT`-Befehl

Zur Überprüfung sollten wir uns mindestens die alten und neuen Attribute einmal anschauen und sortieren, ob es leere Felder gibt, um diese gesondert zu überprüfen. Hat alles geklappt?

```sql
SELECT namen, firstname, lastname  FROM letsMeetMigrationScheme.tblUrsprung ORDER BY lastname ASC;
```

Ein einfacher Weg, die Vollständigkeit eines Imports zu überprüfen, wenn man die Daten kennt, ist das Zählen der Zeichen:

```sql
SELECT SUM(LEN(namen)), SUM(LEN(firstname)),  SUM(LEN(lastname))  FROM letsMeetMigrationScheme.tblUrsprung;
```

|SUM(CHAR_LENGTH(namen))|SUM(CHAR_LENGTH(firstname))|SUM(CHAR_LENGTH(lastname))|
|---|---|---|
|24315|10149|10946|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1.  Lesend die Daten aufbereiten

Um die bestehenden Daten aufzubereiten, müssen wir Zeichenkettenfunktionen bemühen. Eine erste Übersicht für verschiedene DBMS und Links, um die passenden Funktionen aus den Dokumentationen zu finden, ist [hier](https://oer-informatik.de/sql-dml-singlerowfunctions) beschrieben. 

Die wichtigsten Zeichenkettenoperationen der DBMS, die uns hier helfen, sind folgende:


- Teilstrings erzeugen (rechts, links, oder positionsabhängig): `SUBSTRING(...)` (in _PostgreSQL_)

- Zeichenkettenlängen bestimmen: `CHAR_LENGTH()` (in _PostgreSQL_)

- Position von Zeichen in Zeichenketten: `POSITION(...)` (in _PostgreSQL_)

- Zeichen von Whitespaces entfernen: `TRIM(...)` (in _PostgreSQL_)

Für unsere jeweilige Aufgabe müssen wir jetzt nur noch einen passenden Separator finden, mithilfe dessen wir die Zeichenkette zerteilen können.

![Die bisherige Spaltenamen enthält Vorname und Nachname](beispielloesung/images/substring.png)

Im ersten Beispiel ist der Separator ein Komma (plus Leerzeichen), schematisch benötigen wir also etwa folgende Befehle für die Komponenten:

![Am Beispiel einer Zeile skizziert die pos() und substring() Funktion](beispielloesung/images/substring2.png)

PostgreSQL-Beispiel:

```sql
select
 namen, 
 trim(substring ( namen FROM 0 FOR position (', ' IN namen ) )) as lastname,
 trim(substring ( namen FROM (position(', ' IN namen )+2) )) as firstame,
 char_length ( namen ) as Gesamtlaenge,
 position (', ' IN namen ) as Kommaposition,
 char_length ( trim(substring ( namen FROM 0 FOR position (', ' IN namen ) ))) as LaengeNachname,
 char_length ( trim(substring ( namen FROM (position(', ' IN namen )+2) ))) as LaengeVorname
 from letsMeetMigrationScheme.tblUrsprung;
```


2.  Die nötigen neuen Spalten erzeugen

Die Ergebnisse dieser Abfrage speichern wir zunächst in einem neuen Attribut (eine neue Spalte) in `tblUrsprung`. Es vereinfacht das Verfahren erheblich, wenn wir die Werte nicht direkt in die eigentliche Zieltabelle `tblUser` übernehmen, sondern unser Gesamtproblem atomisieren und kleinschrittig lösen. Wir benötigen also neue Attribute und müssen unsere Tabelle anpassen:

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD firstname CHAR(32);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD lastname CHAR(32);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

Es folgt der eigentliche datenverändernde Schritt: Wir übernehmen die Projektion aus den `SELECT`-Befehlen, die wir oben erstellt hatten, und weisen diese Werte den neuen Attributen zu. Durch diese Aufteilung können wir zunächst gefahrlos die Ergebnisse der Projektion begutachten, und erst, wenn wir keine Probleme mehr entdecken, die Daten schreiben.

Aus der Projektion `TRIM(SUBSTR(namen, INSTR(namen,", ")+2)) AS firstname` wird so die Zuweisung `firstname =trim(substring ( namen FROM (position(', ' IN namen )+2) ))`. In die Syntax eines `UPDATE`-Befehls gegossen wird daraus:

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 lastname =trim(substring ( namen FROM 0 FOR position (', ' IN namen ) ) ),
 firstname =trim(substring ( namen FROM (position(', ' IN namen )+2) ));
```

4. Prüfen per `SELECT`-Befehl

Zur Überprüfung sollten wir uns mindestens die alten und neuen Attribute einmal anschauen und sortieren, ob es leere Felder gibt, um diese gesondert zu überprüfen. Hat alles geklappt?

```sql
select namen, firstname, lastname  from letsMeetMigrationScheme.tblursprung ;
```


Ein einfacher Weg, die Vollständigkeit eines Imports zu überprüfen, wenn man die Daten kennt, ist das Zählen der Zeichen:

```sql
SELECT SUM(CHAR_LENGTH(namen)), SUM(CHAR_LENGTH(firstname)),  SUM(CHAR_LENGTH(lastname))  FROM letsMeetMigrationScheme.tblUrsprung;
```

|SUM(CHAR_LENGTH(namen))|SUM(CHAR_LENGTH(firstname))|SUM(CHAR_LENGTH(lastname))|
|---|---|---|
|24735|10307|11208|

</span>

Diese vier Schritte wiederholen sich jetzt für die unterschiedlichen Werte, die wir aufteilen müssen. Ich nenne diese im Folgenden nur noch, ohne sie wieder ausführlich zu beschreiben.

#### Adresse aufteilen

##### PLZ/Ort von Straße/Nr trennen:

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1.  Lesend die Daten aufbereiten

Im Feld `adresse` stecken die Werte von vier Attributen. Wir teilen zunächst in die zwei Gruppen PLZ/Ort und Straße/Hausnummer, um dann in einem zweiten Schritt die einzelnen Werte zu ermitteln:

```sql
SELECT
 adresse, 
 TRIM(SUBSTRING(adresse,1,LOCATE(', ', adresse)-1)) AS street_nr,
 TRIM(SUBSTRING( adresse FROM (LOCATE(', ', adresse)+2) )) AS postcode_city,
 CHAR_LENGTH( adresse ) AS Gesamtlaenge,
 position (', ' IN adresse ) AS Kommaposition,
 CHAR_LENGTH( TRIM(SUBSTRING(adresse,1,LOCATE(', ', adresse)-1))) AS length_street_nr,
 CHAR_LENGTH( TRIM(SUBSTRING( adresse FROM (LOCATE(', ', adresse)+2) ))) AS length_postcode_city
 FROM letsMeetMigrationScheme.tblUrsprung;
```


2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode_city CHAR(128);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_nr CHAR(80);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street_nr = TRIM(SUBSTRING(adresse,1,LOCATE(', ', adresse)-1)),
 postcode_city = TRIM(SUBSTRING( adresse FROM (LOCATE(', ', adresse)+2) )) ;
```


4. Prüfen per `SELECT`-Befehl

Per Sichtung der Ergebnisse:

```sql
SELECT street_nr,  postcode_city, adresse FROM tblUrsprung ORDER BY street_nr ASC;
```

Per Vergleich mit der Summe der Zeichen:
```sql
SELECT SUM(CHAR_LENGTH(street_nr)),  SUM(CHAR_LENGTH(postcode_city)) FROM tblUrsprung;
```

|SUM(CHAR_LENGTH(street_nr))|SUM(CHAR_LENGTH(postcode_city))|
|---|---|
|25036|25170|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

1.  Lesend die Daten aufbereiten

Im Feld `adresse` stecken die Werte von vier Attributen. Wir teilen zunächst in die zwei Gruppen PLZ/Ort und Straße/Hausnummer, um dann in einem zweiten Schritt die einzelnen Werte zu ermitteln:

```sql
SELECT
 adresse, 
 TRIM(LEFT(adresse, PATINDEX('%, %', adresse)-1)) AS street_nr,
 TRIM(RIGHT(RTRIM(adresse), LEN(adresse) - PATINDEX('%, %', adresse)) ) AS postcode_city,
 LEN( adresse ) AS Gesamtlaenge,
 PATINDEX('%, %', adresse) AS Kommaposition,
 LEN(TRIM(LEFT(adresse, PATINDEX('%, %', adresse)-1))) AS length_street_nr,
 LEN(TRIM(RIGHT(RTRIM(adresse), LEN(adresse) - PATINDEX('%, %', adresse)) )) AS length_postcode_city
 FROM letsMeetMigrationScheme.tblUrsprung;
```


2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode_city char(128);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_nr char(80);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street_nr = TRIM(LEFT(adresse, PATINDEX('%, %', adresse)-1)),
 postcode_city = TRIM(RIGHT(RTRIM(adresse), LEN(adresse) - PATINDEX('%, %', adresse)) ) ;
```


4. Prüfen per `SELECT`-Befehl

Per Sichtung der Ergebnisse:

```sql
SELECT street_nr,  postcode_city, adresse FROM letsMeetMigrationScheme.tblUrsprung ORDER BY street_nr ASC;
```

Per Vergleich mit der Summe der Zeichen:
```sql
SELECT 
SUM(LEN(street_nr)),  SUM(LEN(postcode_city)) FROM letsMeetMigrationScheme.tblUrsprung 
```

|SUM(CHAR_LENGTH(street_nr))|SUM(CHAR_LENGTH(postcode_city))|
|---|---|
|24658|24921|


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1.  Lesend die Daten aufbereiten

Im Feld `adresse` stecken die Werte von vier Attributen. Wir teilen zunächst in die zwei Gruppen PLZ/Ort und Straße/Hausnummer, um dann in einem zweiten Schritt die einzelnen Werte zu ermitteln:

```sql
select
 adresse, 
 trim(substring ( adresse FROM 0 FOR position (', ' IN adresse ) )) as street_nr,
 trim(substring ( adresse FROM (position(', ' IN adresse )+2) )) as postcode_city,
 char_length ( adresse ) as Gesamtlaenge,
 position (', ' IN adresse ) as Kommaposition,
 char_length ( trim(substring ( adresse FROM 0 FOR position (', ' IN adresse ) ))) as length_street_nr,
 char_length ( trim(substring ( adresse FROM (position(', ' IN adresse )+2) ))) as length_postcode_city
 from letsMeetMigrationScheme.tblUrsprung;
```


2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode_city CHAR(128);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_nr CHAR(80);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street_nr = trim(substring ( adresse FROM 0 FOR position (', ' IN adresse ) )),
 postcode_city = trim(substring ( adresse FROM (position(', ' IN adresse )+2) ));
```


4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblursprung ;
```

</span>

##### Straße von Nr trennen

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1.  Lesend die Daten aufbereiten

```sql
SELECT
 street_nr,
 TRIM(SUBSTRING(street_nr, REGEXP_INSTR(street_nr, '[^0-9]+'), REGEXP_INSTR(street_nr, '[0-9]+.*')-1) ) AS street,
 TRIM(SUBSTRING(street_nr, REGEXP_INSTR(street_nr, '[0-9]+.*'))) AS street_no
 FROM letsMeetMigrationScheme.tblUrsprung;
``` 


2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_no CHAR(16);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)
```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street = TRIM(SUBSTRING(street_nr, REGEXP_INSTR(street_nr, '[^0-9]+'), REGEXP_INSTR(street_nr, '[0-9]+.*')-1) ),
 street_no = TRIM(SUBSTRING(street_nr, REGEXP_INSTR(street_nr, '[0-9]+.*')));
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT street_nr, street, street_no
FROM letsMeetMigrationScheme.tblUrsprung
ORDER BY street ASC; ;
```

Prüfung anhand der umgewandelten Zeichen:

```sql
SELECT SUM(CHAR_LENGTH(street_nr)), SUM(CHAR_LENGTH(street)), SUM(CHAR_LENGTH(street_no))
FROM letsMeetMigrationScheme.tblUrsprung;
```
|SUM(CHAR_LENGTH(street_nr))|SUM(CHAR_LENGTH(street))|SUM(CHAR_LENGTH(street_no))|
|---|---|---|
|25036|19604|3856|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">


1.  Lesend die Daten aufbereiten

```sql
SELECT
 street_nr,
 TRIM(LEFT(street_nr, PATINDEX('%[0-9]%', street_nr)-1)) AS street,
 TRIM(RIGHT(RTRIM(street_nr), LEN(street_nr)-PATINDEX('%[0-9]%', street_nr)+2)) AS street_no
 FROM letsMeetMigrationScheme.tblUrsprung;
``` 


2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street char(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_no char(16);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)
```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street = TRIM(LEFT(street_nr, PATINDEX('%[0-9]%', street_nr)-1)),
 street_no = TRIM(RIGHT(RTRIM(street_nr), LEN(street_nr)-PATINDEX('%[0-9]%', street_nr)+2));
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT street_nr, street, street_no
FROM letsMeetMigrationScheme.tblUrsprung
ORDER BY street ASC;
```

Prüfung anhand der umgewandelten Zeichen:

```sql
SELECT SUM(LEN(street_nr)), SUM(LEN(street)), SUM(LEN(street_no))
FROM letsMeetMigrationScheme.tblUrsprung;
```
|SUM(LEN(street_nr))|SUM(LEN(street))|SUM(LEN(street_no))|
|---|---|---|
|24658|19226|3856|


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1.  Lesend die Daten aufbereiten

```sql
select
street_nr,
substring (street_nr FROM '[0-9]+.*' ) as street_no,
substring (street_nr FROM '[^0-9]+' ) as street
 from letsMeetMigrationScheme.tblUrsprung;
``` 


2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_no CHAR(16);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)
```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street = trim(substring (street_nr FROM '[^0-9]+' )),
 street_no = trim(substring (street_nr FROM '[0-9]+.*' ));
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT street_nr, street, street_no
FROM letsMeetMigrationScheme.tblUrsprung
ORDER BY street ASC; ;
```

Prüfung anhand der umgewandelten Zeichen:

```sql
SELECT SUM(CHAR_LENGTH(street_nr)), SUM(CHAR_LENGTH(street)), SUM(CHAR_LENGTH(street_no))
FROM letsMeetMigrationScheme.tblUrsprung;
```
|SUM(CHAR_LENGTH(street_nr))|SUM(CHAR_LENGTH(street))|SUM(CHAR_LENGTH(street_no))|
|---|---|---|
|25036|19604|3856|


</span>

##### PLZ von Ort trennen

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1.  Lesend die Daten aufbereiten

```sql
SELECT
 postcode_city,
 TRIM(SUBSTRING(postcode_city, 1, LOCATE(', ', postcode_city)-1 )) AS postcode,
 TRIM(SUBSTRING( postcode_city, LOCATE(', ', postcode_city)+2) ) AS city
 FROM letsMeetMigrationScheme.tblUrsprung;
``` 

2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD city CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode CHAR(16);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 postcode = TRIM(SUBSTRING(postcode_city, 1, LOCATE(', ', postcode_city)-1 )),
 city = TRIM(SUBSTRING( postcode_city, LOCATE(', ', postcode_city)+2) );
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT postcode, city, postcode_city
FROM  letsMeetMigrationScheme.tblUrsprung
ORDER BY city ASC;
```

```sql
SELECT SUM(CHAR_LENGTH(postcode)), SUM(CHAR_LENGTH(city)), SUM(CHAR_LENGTH(postcode_city))
FROM  letsMeetMigrationScheme.tblUrsprung;
```

|SUM(CHAR_LENGTH(postcode))|SUM(CHAR_LENGTH(city))|SUM(CHAR_LENGTH(postcode_city))|
|---|---|---|
|7822|14196|25170|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">


1.  Lesend die Daten aufbereiten

```sql
SELECT
 postcode_city,
 TRIM(LEFT(postcode_city, PATINDEX('%, %', postcode_city)-1 )) AS postcode,
 TRIM(RIGHT(RTRIM(postcode_city), LEN(postcode_city) - PATINDEX('%, %', postcode_city)) ) AS city
 FROM letsMeetMigrationScheme.tblUrsprung;
``` 

2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD city char(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode char(16);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 postcode = TRIM(LEFT(postcode_city, PATINDEX('%, %', postcode_city)-1 )),
 city = TRIM(RIGHT(RTRIM(postcode_city), LEN(postcode_city) - PATINDEX('%, %', postcode_city)) );
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT postcode, city, postcode_city
FROM  letsMeetMigrationScheme.tblUrsprung
ORDER BY city ASC;
```

```sql
SELECT SUM(LEN(postcode)), SUM(LEN(city)), SUM(LEN(postcode_city))
FROM  letsMeetMigrationScheme.tblUrsprung;
```

|SUM(LEN(postcode))|SUM(LEN(city))|SUM(LEN(postcode_city))|
|---|---|---|
|7822|13947|24921|


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">


1.  Lesend die Daten aufbereiten

```sql
select
postcode_city,
 trim(substring (postcode_city FROM 0 FOR position (', ' IN postcode_city ) )) as postcode,
 trim(substring ( postcode_city FROM (position(', ' IN postcode_city )+2) )) as city,
 from letsMeetMigrationScheme.tblUrsprung;
``` 

2.  Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD city CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode CHAR(16);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 postcode = trim(substring (postcode_city FROM 0 FOR position (', ' IN postcode_city ) )),
 city = trim(substring ( postcode_city FROM (position(', ' IN postcode_city )+2) ));
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT postcode, city, postcode_city
FROM  letsMeetMigrationScheme.tblUrsprung
ORDER BY city ASC;
```

```sql
SELECT SUM(CHAR_LENGTH(postcode)), SUM(CHAR_LENGTH(city)), SUM(CHAR_LENGTH(postcode_city))
FROM  letsMeetMigrationScheme.tblUrsprung;
```

|SUM(CHAR_LENGTH(postcode))|SUM(CHAR_LENGTH(city))|SUM(CHAR_LENGTH(postcode_city))|
|---|---|---|
|7822|14196|25170|

</span>

#### Hobbys

##### Die fünf Hobbys voneinander trennen

Bei den Hobbys müssen wir unsere Strategie etwas anpassen, damit die Abfragen nicht zu kompliziert werden:

* Wir trennen mit einer Abfrage immer das vorderste Hobby inkl. Priorität ab (trennen am "; ")

* Den abgetrennten Hobbynamen speichern wir in einem neuen Attribut (erste Runde: `hobby1`, zweite Runde `hobby2` usw.) um hier später Hobbynamen und Priorität zu trennen

* Den Text nach dem vordersten Hobby speichern wir ebenso in einer neuen Spalte `hobbyrest`, die wir dann wiederum in die oberste Abfrage geben und so das folgende Hobby separieren.

Im ganzen gibt es fünf Hobbys, es ist also etwas Copy&Past nötig:

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1. Lesend die Daten aufbereiten

```sql
SELECT
 hobbytext,
 TRIM(SUBSTRING(hobbytext, 1, LOCATE(';', hobbytext)-1 )) AS hobby1,
 TRIM(SUBSTRING(hobbytext,  (LOCATE(';', hobbytext)+1) )) AS hobbyrest
 FROM letsMeetMigrationScheme.tblUrsprung;
```

2. Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobbyrest CHAR(255);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5 CHAR(132);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1 = TRIM(SUBSTRING(hobbytext, 1, LOCATE(';', hobbytext)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbytext,  (LOCATE(';', hobbytext)+1) ))
WHERE LOCATE(';', hobbytext) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT hobbytext, hobby1, hobby2, hobby3, hobby4, hobby5  FROM letsMeetMigrationScheme.tblUrsprung ;
```

```sql
SELECT SUM(CHAR_LENGTH(hobbytext)), SUM(CHAR_LENGTH(hobby1)), SUM(CHAR_LENGTH(hobby2)), SUM(CHAR_LENGTH(hobby3)), SUM(CHAR_LENGTH(hobby4)), SUM(CHAR_LENGTH(hobby5))  FROM letsMeetMigrationScheme.tblUrsprung ;
```

|SUM(CHAR_LENGTH(hobbytext))|SUM(CHAR_LENGTH(hobby1))|SUM(CHAR_LENGTH(hobby2))|SUM(CHAR_LENGTH(hobby3))|SUM(CHAR_LENGTH(hobby4))|SUM(CHAR_LENGTH(hobby5))|
|---|---|---|---|---|
|176484|50743|47866|47866|47866|


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">


1. Lesend die Daten aufbereiten

```sql
SELECT
 hobbytext,
 TRIM(LEFT(hobbytext, PATINDEX('%;%', hobbytext)-1 )) AS hobby1,
 TRIM(RIGHT(RTRIM(hobbytext),  (LEN(hobbytext)-PATINDEX('%;%', hobbytext)) )) AS hobbyrest
 FROM letsMeetMigrationScheme.tblUrsprung;
```

2. Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobbyrest char(255);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5 char(132);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1 = TRIM(LEFT(hobbytext, PATINDEX('%;%', hobbytext)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbytext),  (LEN(hobbytext)-PATINDEX('%;%', hobbytext)) ))
WHERE PATINDEX('%;%', hobbytext) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;

```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT hobbytext, hobby1, hobby2, hobby3, hobby4, hobby5  FROM letsMeetMigrationScheme.tblUrsprung ;
```

```sql
SELECT SUM(LEN(hobbytext)), SUM(LEN(hobby1)), SUM(LEN(hobby2)), SUM(LEN(hobby3)), SUM(LEN(hobby4)), SUM(LEN(hobby5))  FROM letsMeetMigrationScheme.tblUrsprung ;
```

|SUM(LEN(hobbytext))|SUM(LEN(hobby1))|SUM(LEN(hobby2))|SUM(LEN(hobby3))|SUM(LEN(hobby4))|SUM(LEN(hobby5))|
|---|---|---|---|---|---|
|174.464|50.263|49.441|42.295|20.961|3.414|


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1. Lesend die Daten aufbereiten

```sql
select
hobbytext,
 trim(substring (hobbytext FROM 0 FOR position ('; ' IN hobbytext ) )) as hobby1,
 trim(substring (hobbytext FROM (position(', ' IN hobbytext )+2) )) as hobbyrest
 from letsMeetMigrationScheme.tblUrsprung;
```

2. Die nötigen neuen Spalten erzeugen

```sql
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobbyrest CHAR(255);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5 CHAR(132);
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1 = trim(substring (hobbytext FROM 0 FOR position ('; ' IN hobbytext ) )),
hobbyrest = trim(substring (hobbytext FROM (position(', ' IN hobbytext )+2) ))
WHERE position ('; ' IN hobbytext ) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2 = trim(substring (hobbyrest FROM 0 FOR position ('; ' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(', ' IN hobbyrest )+2) ))
WHERE position ('; ' IN hobbytext ) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3 = trim(substring (hobbyrest FROM 0 FOR position ('; ' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(', ' IN hobbyrest )+2) ))
WHERE position ('; ' IN hobbytext ) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4 = trim(substring (hobbyrest FROM 0 FOR position ('; ' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(', ' IN hobbyrest )+2) ))
WHERE position ('; ' IN hobbytext ) >0;
```

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5 = trim(substring (hobbyrest FROM 0 FOR position ('; ' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(', ' IN hobbyrest )+2) ))
WHERE position ('; ' IN hobbytext ) >0;
```

4. Prüfen per `SELECT`-Befehl


```sql
SELECT hobbytext, hobby1, hobby2, hobby3, hobby4, hobby5  FROM letsMeetMigrationScheme.tblUrsprung ;
```

```sql
SELECT SUM(CHAR_LENGTH(hobbytext)), SUM(CHAR_LENGTH(hobby1)), SUM(CHAR_LENGTH(hobby2)), SUM(CHAR_LENGTH(hobby3)), SUM(CHAR_LENGTH(hobby4)), SUM(CHAR_LENGTH(hobby5))  FROM letsMeetMigrationScheme.tblUrsprung ;
```

|SUM(CHAR_LENGTH(hobbytext))|SUM(CHAR_LENGTH(hobby1))|SUM(CHAR_LENGTH(hobby2))|SUM(CHAR_LENGTH(hobby3))|SUM(CHAR_LENGTH(hobby4))|SUM(CHAR_LENGTH(hobby5))|
|---|---|---|---|---|
|176484|50743|47866|47866|47866|


</span>

##### Die Hobby-Prioritäten von den einzelnen Hobbys trennen

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1. Lesend die Daten aufbereiten

Die Bearbeitung kann für die Spalten `hobby1` bis `hobby5` identisch erfolgen - wichtig ist dabei, an allen Stellen des SQL-Befehls auf das korrekte Feld zu verweisen!

```sql
SELECT
 hobby1,
 TRIM(SUBSTRING(hobby1, 1, LOCATE(' %', hobby1) )) as hobby1text,
 CAST(TRIM(TRIM('%' FROM SUBSTRING(hobby1,  (LOCATE(' %', hobby1)+2)) )))AS UNSIGNED INTEGER) AS hobby1PRIO
 FROM letsMeetMigrationScheme.tblUrsprung
 ORDER BY hobby1PRIO
```

2. Die nötigen neuen Spalten erzeugen

```sql
/* Struktur vorbereiten für die Hobby-Prioritäten */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5_prio INTEGER;
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1_prio = CAST( TRIM(TRIM('%' FROM SUBSTRING(hobby1,  (LOCATE(' %', hobby1)+2)) )) AS UNSIGNED INTEGER),
hobby1 = TRIM(SUBSTRING(hobby1, 1, LOCATE(' %', hobby1) ))
WHERE position (' %' IN hobby1 ) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby2,  (LOCATE(' %', hobby2)+2))) )AS UNSIGNED INTEGER),
hobby2 = TRIM(SUBSTRING(hobby2, 1, LOCATE(' %', hobby2) ))
WHERE LOCATE(' %', hobby2) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby3,  (LOCATE(' %', hobby3)+2))) )AS UNSIGNED INTEGER),
hobby3 = TRIM(SUBSTRING(hobby3, 1, LOCATE(' %', hobby3) ))
WHERE LOCATE(' %', hobby3) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby4,  (LOCATE(' %', hobby4)+2))) )AS UNSIGNED INTEGER),
hobby4 = TRIM(SUBSTRING(hobby4, 1, LOCATE(' %', hobby4) ))
WHERE LOCATE(' %', hobby4) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby5,  (LOCATE(' %', hobby5)+2))) )AS UNSIGNED INTEGER),
hobby5 = TRIM(SUBSTRING(hobby5, 1, LOCATE(' %', hobby5) ))
WHERE LOCATE(' %', hobby5) >0;
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblursprung ;
```

```sql
SELECT
 SUM(CHAR_LENGTH(hobby1)), SUM(hobby1_prio),
 SUM(CHAR_LENGTH(hobby2)), SUM(hobby2_prio),
 SUM(CHAR_LENGTH(hobby3)), SUM(hobby3_prio),
 SUM(CHAR_LENGTH(hobby4)), SUM(hobby4_prio),
 SUM(CHAR_LENGTH(hobby5)), SUM(hobby5_prio)
 FROM letsMeetMigrationScheme.tblUrsprung
 ```

| SUM(CHAR_LENGTH(hobby1))|SUM(hobby1_prio)|SUM(CHAR_LENGTH(hobby2))|SUM(hobby2_prio)|SUM(CHAR_LENGTH(hobby3))|SUM(hobby3_prio)|SUM(CHAR_LENGTH(hobby4))|SUM(hobby4_prio)|SUM(CHAR_LENGTH(hobby5))|SUM(hobby5_prio)|
|---|---|---|---|---|---|---|---|---|---|
|42.566|78483|42145|75051|36576|58518|18364|26072|3003|3888|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">



1. Lesend die Daten aufbereiten

Die Bearbeitung kann für die Spalten `hobby1` bis `hobby5` identisch erfolgen - wichtig ist dabei, an allen Stellen des SQL-Befehls auf das korrekte Feld zu verweisen!

```sql
SELECT
 hobby1,
 TRIM(LEFT(hobby1, PATINDEX('% [%]%', hobby1) )) as hobby1text,
 CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby1),  LEN(hobby1) -(PATINDEX('% [%]%', hobby1))) )) AS int) AS hobby1PRIO
 FROM letsMeetMigrationScheme.tblUrsprung
 ORDER BY hobby1PRIO
```

2. Die nötigen neuen Spalten erzeugen

```sql
/* Struktur vorbereiten für die Hobby-Prioritäten */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5_prio int;
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby1),  LEN(hobby1) -(PATINDEX('% [%]%', hobby1))) )) AS int),
hobby1 = TRIM(LEFT(hobby1, PATINDEX('% [%]%', hobby1) ))
WHERE PATINDEX('% [%]%', hobby1) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby2),  LEN(hobby2) -(PATINDEX('% [%]%', hobby2))) )) AS int),
hobby2 = TRIM(LEFT(hobby2, PATINDEX('% [%]%', hobby2) ))
WHERE PATINDEX('% [%]%', hobby2) >0;

```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby3),  LEN(hobby3) -(PATINDEX('% [%]%', hobby3))) )) AS int),
hobby3 = TRIM(LEFT(hobby3, PATINDEX('% [%]%', hobby3) ))
WHERE PATINDEX('% [%]%', hobby3) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby4),  LEN(hobby4) -(PATINDEX('% [%]%', hobby4))) )) AS int),
hobby4 = TRIM(LEFT(hobby4, PATINDEX('% [%]%', hobby4) ))
WHERE PATINDEX('% [%]%', hobby4) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby5),  LEN(hobby5) -(PATINDEX('% [%]%', hobby5))) )) AS int),
hobby5 = TRIM(LEFT(hobby5, PATINDEX('% [%]%', hobby5) ))
WHERE PATINDEX('% [%]%', hobby5) >0;
```

4. Prüfen per `SELECT`-Befehl

```sql
SELECT *  
 FROM letsMeetMigrationScheme.tblUrsprung
 ORDER BY hobby5_prio DESC, hobby4_prio DESC, hobby3_prio DESC, hobby2_prio DESC, hobby1_prio DESC
```

```sql
SELECT
 SUM(LEN(hobby1)), SUM(hobby1_prio),
 SUM(LEN(hobby2)), SUM(hobby2_prio),
 SUM(LEN(hobby3)), SUM(hobby3_prio),
 SUM(LEN(hobby4)), SUM(hobby4_prio),
 SUM(LEN(hobby5)), SUM(hobby5_prio)
 FROM letsMeetMigrationScheme.tblUrsprung
 ```

| SUM(LEN(hobby1))|SUM(hobby1_prio)|SUM(LEN(hobby2))|SUM(hobby2_prio)|SUM(LEN(hobby3))|SUM(hobby3_prio)|SUM(LEN(hobby4))|SUM(hobby4_prio)|SUM(LEN(hobby5))|SUM(hobby5_prio)|
|---|---|---|---|---|---|---|---|---|---|
|42.566|78.483|42.145|75.501|36.576|58.518|18364|26.072|3.003|3.888|

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1. Lesend die Daten aufbereiten

Die Bearbeitung kann für die Spalten `hobby1` bis `hobby5` identisch erfolgen - wichtig ist dabei, an allen Stellen des SQL-Befehls auf das korrekte Feld zu verweisen!

```sql
select
hobby1,
 trim(substring (hobby1 FROM 0 FOR position (' %' IN hobby1 ) )) as hobby1text,
 CAST(trim(trim ( '%' FROM  substring (hobby1 FROM (position(' %' IN hobby1 )+2)  ))) AS INTEGER) as hobbyPRIO
 from letsMeetMigrationScheme.tblUrsprung;
```

2. Die nötigen neuen Spalten erzeugen

```sql
/* Struktur vorbereiten für die Hobby-Prioritäten */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5_prio INTEGER;
```

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1_prio = CAST(trim(trim ( '%' FROM  substring (hobby1 FROM (position(' %' IN hobby1 )+2)  ))) AS INTEGER),
hobby1 = trim(substring (hobby1 FROM 0 FOR position (' %' IN hobby1 ) ))
WHERE position (' %' IN hobby1 ) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2_prio = trim(trim ( '%' FROM  substring (hobby2 FROM (position(' %' IN hobby2 )+2)  ))) AS INTEGER),
hobby2 = trim(substring (hobby2 FROM 0 FOR position (' %' IN hobby2) ))
WHERE position (' %' IN hobby2 ) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3_prio = trim(trim ( '%' FROM  substring (hobby3 FROM (position(' %' IN hobby3 )+2)  ))) AS INTEGER),
hobby3 = trim(substring (hobby3 FROM 0 FOR position (' %' IN hobby3) ))
WHERE position (' %' IN hobby3 ) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4_prio = trim(trim ( '%' FROM  substring (hobby4 FROM (position(' %' IN hobby4 )+2)  ))) AS INTEGER),
hobby4 = trim(substring (hobby4 FROM 0 FOR position (' %' IN hobby4) ))
WHERE position (' %' IN hobby4 ) >0;
```
```sql
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5_prio = trim(trim ( '%' FROM  substring (hobby5 FROM (position(' %' IN hobby5 )+2)  ))) AS INTEGER),
hobby5 = trim(substring (hobby5 FROM 0 FOR position (' %' IN hobby5) ))
WHERE position (' %' IN hobby5 ) >0;
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblursprung ;
```

```sql
SELECT
 SUM(CHAR_LENGTH(hobby1)), SUM(hobby1_prio),
 SUM(CHAR_LENGTH(hobby2)), SUM(hobby2_prio),
 SUM(CHAR_LENGTH(hobby3)), SUM(hobby3_prio),
 SUM(CHAR_LENGTH(hobby4)), SUM(hobby4_prio),
 SUM(CHAR_LENGTH(hobby5)), SUM(hobby5_prio)
 FROM letsMeetMigrationScheme.tblUrsprung
 ```

| SUM(CHAR_LENGTH(hobby1))|SUM(hobby1_prio)|SUM(CHAR_LENGTH(hobby2))|SUM(hobby2_prio)|SUM(CHAR_LENGTH(hobby3))|SUM(hobby3_prio)|SUM(CHAR_LENGTH(hobby4))|SUM(hobby4_prio)|SUM(CHAR_LENGTH(hobby5))|SUM(hobby5_prio)|
|---|---|---|---|---|---|---|---|---|---|
|43046|78483|40557|74184|40557|74184|40557|74184|40557|74184|

</span>

### Die Daten von tblUrsprung in die anderen Tabellen übernehmen

Jetzt sind wir so weit, dass wir uns an die eigentliche Zielstruktur wagen können.

Einige Tabellen sind über `FOREIGN KEY` _Constraints_ miteinander verknüpft. Bevor wir Daten in diese Tabellen eintragen müssen wir also sicherstellen, dass die nötigen Schlüssel in den verknüpften Tabellen existieren (oder - als Workaround - den `FOREIGN KEY` _Constraint_ temporär deaktivieren).

Es empfiehlt sich also - analog zur Erstellung der Struktur - zunächst die unabhängigen Tabellen zu befüllen. Eine Reihenfolge hatten wir bereits in Teil 1 erstellt:

![Abhängigkeit zwischen den Tabellen als Gantt-Diagramm](beispielloesung/plantuml/tabellenreihenfolge.png)


#### `tblGender`


1. Lesend die Daten aufbereiten

Um die unterschiedlichen Geschlechtszuweisungen auszulesen, müssen wir unsere Ursprungstabelle mit einer [Aggregatsfunktion und Gruppierung](https://oer-informatik.de/sql-groupfunctions) zusammenfassen. Auf diesem Weg erhalten wir jedes Geschlecht nur einmal:

```sql
SELECT
tblUrsprung.geschlecht  AS gender
FROM letsMeetMigrationScheme.tblUrsprung
GROUP by tblUrsprung.geschlecht;
```

2. Die nötigen neuen Spalten erzeugen

Die nötige Tabelle wurde bereits in Teil 1 erzeugt (`tblGender`)

3. Die Daten in die neuen Zeilen übernehmen (SELECT in INSERT umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblGender(gender)
SELECT
tblUrsprung.geschlecht  AS gender
FROM letsMeetMigrationScheme.tblUrsprung
GROUP by tblUrsprung.geschlecht;
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblGender ;
```

#### `tblHobby`

1. Lesend die Daten aufbereiten

Unsere Hobbys stecken ja nun in fünf Spalten. Wir wollen diese aber zusammenfassen. Am einfachsten geht das, in dem wir fünf einzelne Abfragen (für jede `hobby*`-Spalte eine) per `UNION` zusammenfügen. Damit wir keine Dopplungen erhalten, nutzen wir das Schlüsselwort `DISTINCT`.

Im Anschluss müssen wir noch die leeren Hobbyeinträge aus dem Abfrageergebnis entfernen. Um Dopplungen schneller entdecken zu können, habe ich die Ergebnisse sortieren lassen.

```sql
SELECT DISTINCT hobby FROM (
(SELECT DISTINCT hobby1 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby2 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby3 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby4 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby5 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
 ) AS ursprung
WHERE hobby IS NOT NULL 
ORDER BY hobby;
```

2. Die nötigen neuen Spalten erzeugen

Die nötige Tabelle wurde bereits in Teil 1 erzeugt (`tblHobby`)

3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `INSERT` umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblHobby(hobbyname)
(SELECT DISTINCT hobby FROM (
(SELECT DISTINCT hobby1 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby2 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby3 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby4 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
UNION
(SELECT DISTINCT hobby5 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung) 
 ) AS ursprung
WHERE hobby IS NOT NULL 
);
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblHobby ;
```

#### tblUser

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1. Lesend die Daten aufbereiten

Bevor wir die Werte aus `tblUrsprung` in der Tabelle `tblUser` speichern können, muss noch der Datentyp des Datums angepasst werden. Im Fall von _PostgreSQL bietet sich hierfür die Funktion `TO_DATE` an ^[[https://www.postgresqltutorial.com/postgresql-date-functions/postgresql-to_date/](https://www.postgresqltutorial.com/postgresql-date-functions/postgresql-to_date/)].  Für einige andere DBMS finden sich [hier Übersichten](https://oer-informatik.de/sql-dml-singlerowfunctions) zu Datumsfunktionen bzw. Links zur Dokumentation.



```sql
SELECT
    tblUrsprung.user_id AS user_id,
    tblUrsprung.firstname AS firstname,
    tblUrsprung.lastname AS lastname,
    tblUrsprung.telefon AS telephone,
    tblUrsprung.street AS street,
    tblUrsprung.street_no  AS street_no,
    tblUrsprung.postcode  AS postcode,
    tblUrsprung.city AS city,
    'Deutschland' AS country,
    tblUrsprung.email AS email,
    tblUrsprung.geschlecht  AS gender,
    STR_TO_DATE(tblUrsprung.geburtstag,'%d.%m.%Y') AS birthday
FROM letsMeetMigrationScheme.tblUrsprung
```

2. Die nötigen neuen Spalten erzeugen

Die nötige Tabelle wurde bereits in Teil 1 erzeugt (`tblUser`)

3. Die Daten in die neuen Zeilen übernehmen (SELECT in INSERT umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblUser
    (user_id,firstname,lastname, telephone, street, street_no, postcode, city, country, email, gender, birthday)
    SELECT
        tblUrsprung.user_id AS user_id,
        tblUrsprung.firstname AS firstname,
        tblUrsprung.lastname AS lastname,
        tblUrsprung.telefon AS telephone,
        tblUrsprung.street AS street,
        tblUrsprung.street_no  AS street_no,
        tblUrsprung.postcode  AS postcode,
        tblUrsprung.city AS city,
        'Deutschland' AS country,
        tblUrsprung.email AS email,
        tblUrsprung.geschlecht  AS gender,
        STR_TO_DATE(tblUrsprung.geburtstag,'%d.%m.%Y') AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung;
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblUser ;
```


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

1. Lesend die Daten aufbereiten

Bevor wir die Werte aus `tblUrsprung` in der Tabelle `tblUser` speichern können, muss noch der Datentyp des Datums angepasst werden. Im Fall von _SQL Server_ bietet sich hierfür die Funktion `CAST`, `CONVERT` oder `parse` an. 

```sql
SELECT
    tblUrsprung.user_id AS user_id,
    tblUrsprung.firstname AS firstname,
    tblUrsprung.lastname AS lastname,
    tblUrsprung.telefon AS telephone,
    tblUrsprung.street AS street,
    tblUrsprung.street_no  AS street_no,
    tblUrsprung.postcode  AS postcode,
    tblUrsprung.city AS city,
    'Deutschland' AS country,
    tblUrsprung.email AS email,
    tblUrsprung.geschlecht  AS gender,
    parse(tblUrsprung.geburtstag as date using 'de-DE' ) AS birthday
FROM letsMeetMigrationScheme.tblUrsprung
```

2. Die nötigen neuen Spalten erzeugen

Die nötige Tabelle wurde bereits in Teil 1 erzeugt (`tblUser`)

3. Die Daten in die neuen Zeilen übernehmen (SELECT in INSERT umformen)

Da das Feld `user_id` mit fortlaufenden Nummern versehen wird, müssen wir temporär erlauben, dass direkt feste IDs eingefügt werden können. Nach der Abfrage deaktivieren wir diese Funktion wieder:

```sql
SET IDENTITY_INSERT letsMeetMigrationScheme.tblUser ON; 
GO

INSERT INTO letsMeetMigrationScheme.tblUser
    (user_id,firstname,lastname, telephone, street, street_no, postcode, city, country, email, gender, birthday)
    SELECT
    tblUrsprung.user_id AS user_id,
    tblUrsprung.firstname AS firstname,
    tblUrsprung.lastname AS lastname,
    tblUrsprung.telefon AS telephone,
    tblUrsprung.street AS street,
    tblUrsprung.street_no  AS street_no,
    tblUrsprung.postcode  AS postcode,
    tblUrsprung.city AS city,
    'Deutschland' AS country,
    tblUrsprung.email AS email,
    tblUrsprung.geschlecht  AS gender,
    parse(tblUrsprung.geburtstag as date using 'de-DE' ) AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung;
GO

SET IDENTITY_INSERT letsMeetMigrationScheme.tblUser OFF;
GO
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblUser ;
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1. Lesend die Daten aufbereiten

Bevor wir die Werte aus `tblUrsprung` in der Tabelle `tblUser` speichern können, muss noch der Datentyp des Datums angepasst werden. Im Fall von _PostgreSQL bietet sich hierfür die Funktion `TO_DATE` an ^[[https://www.postgresqltutorial.com/postgresql-date-functions/postgresql-to_date/](https://www.postgresqltutorial.com/postgresql-date-functions/postgresql-to_date/)].  Für einige andere DBMS finden sich [hier Übersichten](https://oer-informatik.de/sql-dml-singlerowfunctions) zu Datumsfunktionen bzw. Links zur Dokumentation.



```sql
SELECT
 tblUrsprung.user_id AS user_id,
     tblUrsprung.firstname AS firstname,
     tblUrsprung.lastname AS lastname,
     tblUrsprung.telefon AS telephone,
     tblUrsprung.street AS street,
     tblUrsprung.street_no  AS street_no,
     tblUrsprung.postcode  AS postcode,
     tblUrsprung.city AS city,
     'Deutschlang' AS country,
     tblUrsprung.email AS email,
     tblUrsprung.geschlecht  AS gender,
     TO_DATE(tblUrsprung.geburtstag,'DD.MM.YYYY') AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung
```

2. Die nötigen neuen Spalten erzeugen

Die nötige Tabelle wurde bereits in Teil 1 erzeugt (`tblUser`)

3. Die Daten in die neuen Zeilen übernehmen (SELECT in INSERT umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblUser 
    (user_id,firstname,lastname, telephone, street, street_no, postcode, city, country, email, gender, birthday)
    SELECT
        tblUrsprung.user_id AS user_id,
        tblUrsprung.firstname AS firstname,
        tblUrsprung.lastname AS lastname,
        tblUrsprung.telefon AS telephone,
        tblUrsprung.street AS street,
        tblUrsprung.street_no  AS street_no,
        tblUrsprung.postcode  AS postcode,
        tblUrsprung.city AS city,
        'Deutschland' AS country,
        tblUrsprung.email AS email,
        tblUrsprung.geschlecht  AS gender,
        TO_DATE(tblUrsprung.geburtstag,'DD.MM.YYYY') AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung;
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblUser ;
```

</span>

#### tblHobby2User

1. Lesend die Daten aufbereiten

Um die Zuweisungen der Hobbys zu Usern zu erhalten müssen wir - analog zu oben - wieder alle fünf Hobbyspalten auswerten. In jeder Subquery fügen wir einen `JOIN` mit dem Hobbytext einer dieser fünf Spalten durch und fügen die Ergebnisse wieder per `UNION` zusammen:

```sql
 SELECT hobby_id, user_id, prio 
    FROM (
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby1_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby1)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby2_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby2)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby3_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby3)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby4_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby4)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby5_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby5)
    ) AS T
WHERE T.prio IS NOT NULL;
```

2. Die nötigen neuen Spalten erzeugen

Die nötige Tabelle wurde bereits in Teil 1 erzeugt (`tblHobby2User`)


3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblHobby2User
 (hobby_id,user_id,priority)
 SELECT hobby_id, user_id, prio 
    FROM (
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby1_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby1)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby2_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby2)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby3_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby3)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby4_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby4)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby5_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby5)
    ) AS T
WHERE T.prio IS NOT NULL;
```
4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblHobby2User ;
```

</span>

#### tblGenderInterest

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


1. Lesend die Daten aufbereiten

Abschließend müssen wir noch das Geschlecht, an dem die jeweiligen User Interesse haben, extrahieren. Da sich in der Spalte mehrere Bezeichnungen finden können, müssen wir als `JOIN`-Bedingung prüfen, ob die jeweilige Geschlechtsbezeichnung in der Spalte enthalten ist (PostgreSQL `ILIKE` mit den Wildcards '%'):

```sql
SELECT  letsMeetMigrationScheme.tblGender.gender, letsMeetMigrationScheme.tblUrsprung.user_id
FROM letsMeetMigrationScheme.tblUrsprung
LEFT JOIN letsMeetMigrationScheme.tblGender
ON (letsMeetMigrationScheme.tblUrsprung.interesse LIKE CONCAT('%', letsMeetMigrationScheme.tblGender.gender, '%'));
```

2. Die nötigen neuen Spalten erzeugen


3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblGenderInterest
(gender, user_id)
SELECT  letsMeetMigrationScheme.tblGender.gender, letsMeetMigrationScheme.tblUrsprung.user_id
FROM letsMeetMigrationScheme.tblUrsprung
LEFT JOIN letsMeetMigrationScheme.tblGender
ON (letsMeetMigrationScheme.tblUrsprung.interesse LIKE CONCAT('%', letsMeetMigrationScheme.tblGender.gender, '%'));
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblGenderInterest ;
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">


1. Lesend die Daten aufbereiten

Abschließend müssen wir noch das Geschlecht, an dem die jeweiligen User Interesse haben, extrahieren. Da sich in der Spalte mehrere Bezeichnungen finden können, müssen wir als `JOIN`-Bedingung prüfen, ob die jeweilige Geschlechtsbezeichnung in der Spalte enthalten ist (PostgreSQL `ILIKE` mit den Wildcards '%'):

```sql
SELECT  letsMeetMigrationScheme.tblGender.gender, letsMeetMigrationScheme.tblUrsprung.user_id
FROM letsMeetMigrationScheme.tblUrsprung
LEFT JOIN letsMeetMigrationScheme.tblGender
ON (letsMeetMigrationScheme.tblUrsprung.interesse LIKE '%'+TRIM(letsMeetMigrationScheme.tblGender.gender)+'%')
```

2. Die nötigen neuen Spalten erzeugen


3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)

```sql
INSERT INTO letsMeetMigrationScheme.tblGenderInterest
(gender, user_id)
SELECT  letsMeetMigrationScheme.tblGender.gender, letsMeetMigrationScheme.tblUrsprung.user_id
FROM letsMeetMigrationScheme.tblUrsprung
LEFT JOIN letsMeetMigrationScheme.tblGender
ON (letsMeetMigrationScheme.tblUrsprung.interesse LIKE '%'+TRIM(letsMeetMigrationScheme.tblGender.gender)+'%');
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblGenderInterest ;
```


</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

1. Lesend die Daten aufbereiten

Abschließend müssen wir noch das Geschlecht, an dem die jeweiligen User Interesse haben, extrahieren. Da sich in der Spalte mehrere Bezeichnungen finden können, müssen wir als `JOIN`-Bedingung prüfen, ob die jeweilige Geschlechtsbezeichnung in der Spalte enthalten ist (PostgreSQL `ILIKE` mit den Wildcards '%'):

```sql
select letsmeetmigrationscheme.tblursprung.interesse, letsmeetmigrationscheme.tblursprung.user_id, letsmeetmigrationscheme.tblgender.gender 
from letsmeetmigrationscheme.tblursprung
left join letsmeetmigrationscheme.tblgender 
on (letsmeetmigrationscheme.tblursprung.interesse ILIKE '%' || letsmeetmigrationscheme.tblgender.gender || '%');
```

2. Die nötigen neuen Spalten erzeugen


3. Die Daten in die neuen Zeilen übernehmen (`SELECT` in `UPDATE` umformen)
```sql
insert into letsMeetMigrationScheme.tblGenderInterest
(gender, user_id)
select  letsmeetmigrationscheme.tblgender.gender, letsmeetmigrationscheme.tblursprung.user_id  
from letsmeetmigrationscheme.tblursprung
left join letsmeetmigrationscheme.tblgender 
on (letsmeetmigrationscheme.tblursprung.interesse ILIKE '%' || letsmeetmigrationscheme.tblgender.gender || '%');
```

4. Prüfen per `SELECT`-Befehl

```sql
select *  from letsMeetMigrationScheme.tblGenderInterest ;
```

</span>

### Fazit der Normalisierung

Damit wären die Daten der Ausgangstabelle in `tblUrsprung` angepasst, getrennt, auf Tabellen aufgeteilt. Wir sind also weitgehend fertig. Was fehlt ist eine eingehende Prüfung, ob wirklich alles so geklappt hat, wie es soll.

Ein fertiges SQL-Skript, dass die bestehende Datenbank anpasst für PostgreSQL findet sich hier: 

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_3_normierung_mariadb.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_3_normierung_mariadb.sql)

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_3_normierung_mssqlserver.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_3_normierung_mssqlserver.sql)

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_3_normierung_postgre.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_3_normierung_postgre.sql)
</span>

### Eine gesamte lauffähige Struktur im DB-Containers 

Einen Container, in dem die fertige Datenbank enthalten sind, lässt sich mit folgenden Befehlen erstellen:

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

Variante MariaDB (Anleitung zu MariaDB im Container [hier](https://oer-informatik.de/docker-install-mariadb)). Die folgenden Befehle funktionieren in der PowerShell, für die Linux-Bash muss der `-OutFile "..."`-Teil weggelassen werden.

```powershell
cd ~/Documents
mkdir mariadbdocker
cd mariadbdocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql -OutFile "letsmeet_1_struktur_mariadb.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql  -OutFile "letsmeet_2_import_einzeiler_mariadb.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_3_normierung_mariadb.sql -OutFile "letsmeet_3_normierung_mariadb.sql"
```

Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen MariaDB-Container erzeugen und starten:

```powershell
docker run --name=letsmeetmaria -d -p 3307:3306 -e MARIADB_USER=dbuser -e MARIADB_PASSWORD=_hier%1auchGutesPWnu+2en -e MARIADB_ROOT_PASSWORD=_hier%1auchGutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d mariadb:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) MariaDB-Kommandozeile (`mariadb`) geöffnet werden können:

```powershell
docker exec -it letsmeetmaria mariadb -uroot -p
```

Nach Eingabe des Passworts sieht das Prompt etwa wie folgt aus:

```bash
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.9.3-MariaDB-1:10.9.3+maria~ubu2204 mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```

Mit der folgenden Abfolge von SQL-Befehlen können die vorhandenen Datenbanken ausgegeben werden, die frisch angelegte `letsMeetMigrationScheme` ausgewählt werden und deren Tabellen angezeigt werden:

```sql
SHOW DATABASES;
USE letsMeetMigrationScheme;
SHOW TABLES;
```

Und dieser Befehl sollte jetzt eine gefüllte Tabelle ausgeben:

```sql
SELECT * FROM letsMeetMigrationScheme.tblUrsprung;
```

```
1576 rows in set (0.007 sec)
```

Mit `exit` kann die SQL-Konsole wieder verlassen werden.

Weitere Details im Umgang mit MariaDB im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-mariadb).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```powershell
cd ~/Documents
mkdir mssqlserverdocker
cd mssqlserverdocker
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/Dockerfile -OutFile "Dockerfile"
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/configure-db.sh -OutFile "configure-db.sh"
wget https://gitlab.com/oer-informatik/db-sql/dbms/-/raw/main/mssql/mssql-custom-dockerfile/entrypoint.sh -OutFile "entrypoint.sh"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql -OutFile "letsmeet_1_struktur_mssqlserver.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql -OutFile "letsmeet_2_import_einzeiler_mssqlserver.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_3_normierung_mssqlserver.sql -OutFile "letsmeet_3_normierung_mssqlserver.sql"
```

In diesem Verzeichnis muss jetzt ein `dockerimage` gebaut werden:

```powershell
PS> docker build -t mssql-custom .
```

Mit diesem Image erzeugen wir jetzt einen MS-SQL-Server-Container und starten diesen:

```powershell
PS> docker run --name=letsmeetmssql --hostname=letsmeetmssql -p 3309:1433 -e ACCEPT_EULA=Y -e MSSQL_SA_PASSWORD=_hier%1GutesPWnu+2en -d mssql-custom
```

Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) SQL-Server-Kommandozeile (`sqlcmd`) für Nutzer `SA` mit dem eingegebenen Passwort geöffnet werden können:

```powershell
docker exec -it letsmeetmssql /opt/mssql-tools/bin/sqlcmd -S localhost -U SA
```

Und ein Befehl wie dieser hier sollte den Inhalt der Ausgangsdaten ausgeben:

```sql
SELECT * FROM letsmeetdb.letsMeetMigrationScheme.tblUrsprung;
GO
```

Weitere Details im Umgang mit SQL-Server im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-mssql).

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

Die folgenden Befehle sind für die PowerShell. In der Linux-Bash muss der `-OutFile "..."`-Teil weggelassen werden!

```powershell
cd ~/Documents
mkdir psqldocker
cd psqldocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/blob/raw/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql -OutFile "letsmeet_1_struktur_ddl_postgre.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/blob/raw/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql  -OutFile "letsmeet_2_import_einzeiler_postgre.sql"
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/blob/raw/beispielloesung/postgresql/letsmeet_3_normierung_postgre.sql -OutFile "letsmeet_3_normierung_postgre.sql"
```

Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen postgreSQL-Container erzeugen und starten:

```powershell
docker run --name=letsmeetpsql -d -p 3312:5432 -e POSTGRES_PASSWORD=_hier%1GutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d postgres:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Das dauert diesmal etwas, weil 1576 Zeilen importiert werden müssen. Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) PostgreSQL-Kommandozeile (`psql`) für Nutzer `postgres` mit Passwort `postgres` geöffnet werden können:

```powershell
docker exec -it letsmeetpsql psql -U postgres postgres
```

Die psql-Kommandozeile sieht etwa so aus:

```bash
psql (15.0 (Debian 15.0-1.pgdg110+1))
Type "help" for help.

postgres=# 
```

Und ein Befehl wie dieser hier sollte im Idealfall die Anzahl der Zeilen (1576) ausgeben:

```sql
SELECT COUNT(*) FROM letsMeetMigrationScheme.tblUrsprung;
```

Mit `\q` kann die Konsole wieder verlassen werden.

Weitere Details im Umgang mit PostgreSQL im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-postgres).

</span>

### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)

