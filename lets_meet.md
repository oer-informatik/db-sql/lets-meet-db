## Lernsituation: Migration des "Let’s Meet"-Datenbankprojekts (LS Aufgabenstellung)

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/lets-meet</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110207682728246503</span>

> **tl/dr;** _(ca. 8 min Lesezeit): Es ist nur noch eine Gesamtübersicht einer Datenbank in einer Tabelle vorhanden. Aus diesen Daten soll die ursprüngliche Datenbank wiederhergestellt werden. Die Datenbank soll modelliert, die Daten importiert und im Bestand normalisiert werden. (DB Entwurf, Normalisierung, SQL-DDL, SQL-DML, Container, DBMS-Frontends und -Backends)._

### Ausgangslage:

Ihr Unternehmen hat eine neue Kundin gewonnen: die _Let’s Meet GmbH_, die eine Dating- und Meeting-Plattform betreibt.
Ihr Unternehmen soll die Dating-App der Kundin weiterentwickeln. Leider erfolgte die Trennung vom vorigen IT-Dienstleister im Streit und nicht reibungsfrei, sodass die Kundin keinen Zugriff auf die bestehenden Datenbanken hat. Es liegt lediglich ein Datenbank-Dump in Form einer Excel-Datei vor, die die Inhalte aller Tabellen erfasst.

![Ausschnitt aus "Tea with friends, and one must wear one's finest hat!" (public domain)](images/tea-with-friends_public-domain.png)

Ihr Team ist für die Datenanalyse und den Entwurf der neuen Datenbank sowie die Migration in neue Tabellen zuständig. Die eigentliche App wird von einem anderen Team betreut.

Ausgangslage ist der vorliegende Export der Datenbank als Excel-Tabelle. Um Problemen beim Ändern, Löschen und Einfügen von Datensätzen vorzubeugen, legt die Kundin sehr viel Wert darauf, im Vorfeld der Migration ein ausgefeiltes konzeptuelles Datenmodell zu erhalten. Anhand des Datenmodells soll die Kundin im Anschluss hinsichtlich der Wahl eines logischen Datenmodells und schließlich hinsichtlich eines Datenbankmanagementsystems beraten werden. Um nicht ein weiteres Mal ohne aussagekräftige Dokumente dazustehen, wünscht sich die Kundin zudem eine geeignete Dokumentation der einzelnen Entwurfsphasen und der reproduzierbaren Zwischenschritte. 

Alle genutzten SQL-Befehle sollen versioniert und in einer _Readme.md_ im _git_-Repository dokumentiert werden.

Das Zielsystem soll ein Datenbanksystem (DBS) in einem Container sein, das per _Infrastructure as code_ so angelegt wurde. Eine lauffähige DBS-Umgebung soll so jederzeit erneut erstellt werden können. Für den Fall, dass bei Umwandlungsschritten Fehler gemacht wurden, soll es die Möglichkeit geben, ein DBS aus den Importbefehlen der Ursprungsdaten und allen datenändernden SQL-Befehle jederzeit zügig wiederherzustellen.

Zur Realisierung soll am Ende die vorhandene Datenstruktur in ein Datenbankmanagementsystem (DBMS) im Container realisiert, die Datenbanken importiert, in der Datenbank normalisiert und verknüpft werden. Das so entstehende Datenbanksystem soll jederzeit reproduzierbar an Hand der Angaben im Readme erstellt werden können. 

Alle nötigen Abfragen sollen erstellt und geprüft werden. Aus Kompatibilitätsgründen soll am Ende auch eine Abfrage erzeugt werden, die exakt den Aufbau der ursprünglichen Excel-Tabelle erzeugt.

### Auszug der Daten

Alle Daten finden sich in [dieser Excel-Datei](http://oer-informatik.gitlab.io/db-sql/lets-meet-db/let_s-meet-db-dump.xlsx). Der Aufbau der Daten lässt sich an folgendem Schema erkennen:

* Spalte 1: Name kommagetrennt: Vorname, Nachname

* Spalte 2: Adresse kommagetrennt (Straße / Nr. durch Leerzeichen)

* Spalte 3: Telefonnummern, ggf. kommagetrennt

* Spalte 4: fünf Hobbies, getrennt durch Semikolon. Die Priorität des jeweiligen Hobbys (0-100) wird zwischen zwei Prozentzeichen angegeben.

* Spalte 5: E-Mail-Adresse

* Spalte 6: Gelesenes Geschlecht  (m / w / nicht binär)

* Spalte 7: Interessiert an Geschlecht (m/w/nicht binär, Mehrfachnennung möglich)

* Spalte 8: Geburtsdatum

|Nachname, Vorname	|Straße Nr, PLZ, Ort	|Telefon	|Hobby1 %Prio1%; Hobby2 %Prio2%; Hobby3 %Prio3%; Hobby4 %Prio4%; Hobby5 %Prio5%;	|E-Mail	|Geschlecht	|Interessiert an| Geburtsdatum|
|---|---|---|---|---|---|---|---|
|Gehlen, Ursula	|Papendamm 27, 52062, Aachen	|(0251) 340233	|Gäste einladen %91%; Geschenke machen %75%; Für sich selbst Dinge einkaufen %75%; Eine Blume oder Pflanze sehen oder daran riechen %21%; Leger gekleidet sein %84%; |ursula.gehlen@d-ohnline.te	|w	|m| 13.05.1983|
|Gehrmann, Kai	|Parkstr 6, 52072, Aachen	|(0251) 376775	|Etwas neuartig und originell machen %45%; Ausgiebig frühstücken %40%; An technischen Dingen arbeiten (Fahrzeuge, Hausgeräte usw.) %49%; 	|kai.gehrmann@autluuk.te	|m	|w|11.06.1991|

### Hinweise und Anforderungen an die Realisierung

Diese Lernsituation soll als geführtes Projekt durchgeführt werden, bei dem einige Planungsschritte bereits vorgegeben werden. Zum einen hat die Erfahrung gezeigt, dass es ohne diese Unterstützung nicht in der geforderten Zeit durchführbar ist, zum anderen sollen bewusst die fachgerechten Phasen der Datenbankmodellierung nochmals durchgegangen werden, die für Fachinformatiker_innen prüfungsrelevant sind.

Kleingruppen (3-4 Personen) sollen die Anpassungen planen, durchführen, jedes Zwischenergebnis testen und alle zugrundeliegenden Schritte dokumentieren.

Die folgenden Schritte sollen ausgeführt und im _git_-Repository versioniert und dokumentiert werden:

* **Konzeptuelles Datemmodell**: Analyse der bestehenden Daten in der Excel-Tabelle und Extrahierung eines konzeptuellen Datenmodells (z.B. ER-Diagramm) der Zieldatenbank.

* **Logisches Datenmodell**: Anwenden der Transformationsregeln, um ein konzeptuellen Modell (Entity Relationship Model) in ein logisches Modell (Relationenmodell) umzuwandeln.

* **Datenschutz**: Was ist erforderlich, um die betreffenden Daten verarbeiten und speichern zur dürfen? Aus Sicht des Datenschutzes: welche unterschiedlichen Arten von Daten liegen hier vor und wie müssen sie demnach geschützt werden? Welche Maßnahmen müssen ergriffen werden?

* Normalisierung des Relationenmodells bis zur dritten Normalform (bzw. Dokumentation an welchen Stellen und warum gegen eine Normalform bewusst verstoßen wurde)

* Auswahl eines Datenbankmanagementsystems und Installation der nötigen Infrastruktur

* Erstellung des physischen Datenmodells für die Ursprungsdaten und die Zieldatenbank (SQL-DDL, `CREATE TABLE...`)

* Erstellung der Importbefehle aus der Excel-Datei unter Beachtung der vorhandenen Zeichencodierung (SQL-DML `INSERT INTO ...`)

* Planung der Umwandlung: erfahrungsgemäß ist hier kleinschrittiges iteratives Vorgehen zielführend, das aus vielen lesenden und wenig schreibenden Operationen besteht, z.B:

  * SELECT-Befehle erzeugen, die einzelne Felder mit Mengen/Listen trennen

  * Mehrere SELECT-Befehle kombinieren, damit diese den Inhalt ausgeben, der später in eine Zieltabelle soll

  * Diese SELECT-Befehle für wenige datenverändernde UPDATE oder INSERT-Befehle nutzen, hierbei kann es hilfreich sein, vor dem Zielsystem zunächst Zwischentabellen zu erzeugen.

* Als Akzeptanzkriterium werden am Ende die Anzahlen der Einträge zwischen den Gruppen verglichen sowie in einem Befehl die Ursprungstabelle mit der geforderten Abfrage, die dieses Format wieder erzeugt.

* Welche Abfragen ergeben sich aus den optionalen Anforderungen (siehe unten)?

### Erweiterung der Datenbank

Optional soll die Datenbank für folgende Anwendungsfälle Abfragen und eine Struktur bieten:

- Benutzer*innen können die Hobbys priorisieren, die sie an anderen interessieren (0-100) bzw. die sie ausgesprochen nicht mögen (-100 - 0 )

- Benutzer*innen können andere Benutzer*innen auf ihre „Freundesliste“ setzen.

- Benutzer*innen haben ein Profilbild. Dieses wird direkt in der Datenbank gespeichert (`BLOB`).

- Neben einem Profilbild können Benutzer*innen weitere Fotos hochladen oder verlinken.

Es wurde das unten abgedruckte Anwendungsfalldiagramm erstellt. Diese Anwendungsfälle sollen bereits im Datenmodell vorgesehen werden.

![Anwendungsfalldiagramm für die Let's Meet-DB](plantuml/use-case.png)



### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)

