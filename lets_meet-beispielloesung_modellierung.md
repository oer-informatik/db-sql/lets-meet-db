## Modellierung des "Let’s Meet"-Datenbankprojekts (LS Beispiellösung Teil 1)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/lets_meet-beispielloesung_modellierung</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110207682728246503</span>

> **tl/dr;** _Dies ist der erste Teil (Modellierung) einer Beispiellösung für die ["Let's Meet" Datenbank-Lernsituation](https://oer-informatik.de/lets-meet). Es führen viele Wege zum Ziel - der einzig richtige ist der Weg, den man selbstständig gegangen ist. Daher ist diese Variante bestenfalls als eine Ideenskizze zu verstehen, die genutzt werden kann, wenn man nicht mehr weiterkommt. Einige Schritte habe ich bewusst etwas umständlicher realisiert, um so bestimmte Techniken des Datenbankentwurfs bzw. Abfragetechniken darstellen zu können._


Dieser Artikel ist Teil einer mehrteiligen Serie, in der

- [die eigentliche Aufgabestellung](https://oer-informatik.de/lets-meet) das Ausgangsszenario vorstellt und den Datenbank-Export als Tabelle verlinkt,

- im [ersten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) das Zielsystem modelliert wird (ER-Modell, Relationenmodell, physisches Modell / DDL),

- im [zweiten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_import) SQL-Befehle für den Import aus der Tabellenkalkulation erstellt werden und

- im [dritten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) die Ausgangsdaten über Zeichenkettenfunktionen, Datumsfunktionen, JOINS und Vereinigung von Abfragen schließlich Daten in das neue Zielsystem übernommen werden.

Allen Artikel enden mit einem SQL-Skript, mit dem Daten als Ausgangspunkt für die nächste Etappe genutzt werden können, falls es zu Problemen bei der Eigenentwicklung kam.

### Ausgangslage

Die eigentliche Aufgabestellung [der "Let's Meet" Datenbank-Lernsituation kann hier](https://oer-informatik.de/lets-meet) nachgelesen werden. Ein Datenbankdump liegt als [Excel-Datei (hier)](http://oer-informatik.gitlab.io/db-sql/lets-meet-db/let_s-meet-db-dump.xlsx) vor ist wirklich von unerträglicher Qualität - offensichtlich entspringt er einer Exportabfrage, die zwar alle Daten enthält, jedoch keine sinnvolle Struktur aufweist. Zudem sind die Inhalte redundant gespeichert. Wir müssen hier wohl auf vielen Ebenen Probleme lösen (siehe Beispielauszug):

|Nachname, Vorname|Straße Nr, PLZ Ort|Telefon|Hobby1 %Prio1%; Hobby2 %Prio2%; Hobby3 %Prio3%; Hobby4 %Prio4%; Hobby5 %Prio5%;|E-Mail|Geschlecht (m/w/nonbinary)|Interessiert an|Geburtsdatum|
|---|---|---|---|---|---|---|---|
|Forster, Martin|Minslebener Str. 0, 46286, Dorsten|02372 8020|Fremdsprachenkenntnisse erweitern %78%; Im Wasser waten %80%; Schwierige Probleme klären %61%; Morgens Früh aufstehen %17%; |martin.forster@web.ork|m|w|07.03.1959|
|Elina, Tsanaklidou|Gartenweg 13, 69126, Heidelberg|06221 / 98689|Jemanden massieren %57%; Mir die Probleme von anderen anhören %21%; Abends seinem Partner Ereignisse des Tages erzählen %53%; Für einen guten Zweck spenden %25%; |tsanaklidou.elina@1mal1.te|nb|m|28.02.1958|


### Problemanalyse

Offensichtlich gibt es eine Reihe von Schritten, die erforderlich sind:

- Es muss eine sinnvolle Zieldatenbank entworfen werden,

- die vorliegenden Daten müssen in die Struktur der Zieldatenbank gebracht werden 

- und die Daten müssen in diese Zieldatenbank importiert werden.

Während der Entwurf des Zielsystems noch recht einfach zu realisieren ist, gibt es für den Import der Daten in diese Struktur sehr viele Varianten, die sich in zwei grundlegende Strategien zusammenfassen lassen:

- Einlesen und Anpassen der Daten per Script. Die Logik, die die Daten aufteilt, findet im Script statt.

- Importieren der Ursprungsdaten in eine Tabelle und Normalisierung / Umformung der Daten per SQL-Befehl.

Für beide Varianten gibt es Vor- und Nachteile. Die Aufgabenstellung sieht jedoch vor, dass die Ausgangs-Datenstruktur zunächst importiert werden sollen und daraufhin mit weiteren SQL-Statements in die Zielstruktur überführt werden sollen.

### Planung Teil 1: Datenbankentwurf des Zielsystems

- [Konzeptuelles Datenmodell: Entity-Relationship-Modell](https://oer-informatik.de/erm) aus bestehender Excel-Tabelle extrahieren

- Logisches Datenmodell auswählen (Spoiler: relationale DB und [Relationenmodell](https://oer-informatik.de/relationenmodell) erstellen

- Relationenmodell [normalisieren](https://oer-informatik.de/normalisierung-von-logischen-datenmodellen)

- [Datenbankmanagementsystem](https://oer-informatik.de/dbms) auswählen

- Physisches Datenmodell: SQL-DDL-Befehle für Ursprungsdaten und Ziel-Datenbank erstellen


Planung Teil 2: Die Ursprungsdaten importieren (Details in einem zweiten Schritt, später)

Planung Teil 3: Die Ursprungsdaten in das Zielsystem überführen (Details in einem dritten Schritt, später)

### Konzeptuelles Datenmodell: Welche Daten befinden sich in der Excel-Datei

Aus der Ursprungstabelle lassen sich direkt zwei Entitätstypen ableiten: _Hobby_ und _User_. Eine Reihe von Attributen geben die Spaltenüberschriften unmittelbar her, davon sind einige mehrwertig (Name, Address):

![Entity-Relationship-Diagramm der Let's Meet DB mit den Entitätstypen User und Hobby und mehrwertigen Attributen](beispielloesung/images/erd-simple.png)

Die Beziehung zwischen User und Hobby hat selbst ein Attribut (Priorität), es handelt sich bei der Relationship also um einen assoziativen Entitätstypen (Rechteck mit Raute). Da dieser Entitätstyp ohne _User_ und ohne _Hobby_ nicht existiert, habe ich ihn als schwachen Entitätstypen notiert (doppeltes Rechteck). 

Außerdem müssen die mehrwertigen Attribute noch aufgelöst werden. _Gender_ wird zudem ausgelagert, da es auf zwei verschiedene Arten berücksichtigt werden muss: Das Geschlecht, als das ein User gelesen werden will, sowie das Geschlecht der anderen User, an dem ein User Interesse hat. Mit den nötigen Kardinalitäten versehen sieht das Ganze etwa so aus:  

![Entity-Relationship-Diagramm der Let's Meet DB mit den Entitätstypen User, Hobby, Gender](beispielloesung/images/erd.png)

Bleiben noch die Erweiterungen, die im Datenmodell berücksichtigt werden sollen: 

- das Profilbild wird als neuer Entitätstyp _Picture_ mit einer 1:1 Relationship zu Users notiert

- die Fotos werden über denselben Entitätstyp modelliert, jedoch mit einer 1:n Relationship

- Freundesbeziehungen werden als m:n Relationship umgesetzt

- Hobbies der anderen und deren Priorität analog zu den eigenen Hobbies mit einem schwachen assoziativen Relationshiptypen.

Die Erweiterung des Modells sieht somit so aus:

![Entity-Relationship-Diagramm der Let's Meet DB mit den zusätzlichen Entitätstypen Pictures und "Meets people with" und der Relationship "Friendship"](beispielloesung/images/erd-extended.png)

Um die einzelnen Kardinalitäten und Optionalitäten festzulegen, werden die einzelnen Beziehungen mit Hilfe von ERDish formuliert. ERDish formuliert je Relationship zwei Sätze nach dem Muster:

EACH _Entity A_ `MUST` | `MAY` _Relationship_ `ONE AND ONLY ONE` | `ONE OR MORE` _Entity B_.

Im Einzelnen ergeben sich also für jede Relationship zwischen zwei Entitätstypen zwei Sätze:

#### User - Hobby

EACH _User_ `MAY` _like_ `ONE OR MORE` _Hobby_.

EACH _Hobby_ `MAY` _be liked by_ `ONE OR MORE` _User_.


EACH _User_ `MAY` _like people having_ `ONE OR MORE` _Hobby_.

EACH _Hobby_ `MAY` _of persons may liked by _ `ONE OR MORE` _User_.


#### User - Gender

EACH _User_ `MAY` _be seen as_ `ONE AND ONLY ONE` _Gender_.

EACH _Gender A_ `MAY` _be the gender of_ `ONE OR MORE` _User_.

Ich gehe also bei diesem Modell davon aus, dass die Geschlechtszuordnung eindeutig ist und sich in Schubladen einteilen lässt (z.B.  _maskulin_ / _feminin_ /  _non-binary_, aber diese Liste ist offen für Erweiterungen). 

Zeitgemäßer ist es, anstelle des Geschlechts einfach die Pronomen oder die Anrede zu erfassen. Da für viele auf einer Dating-Plattform das Geschlecht eine Rolle spielt, habe ich das zunächst so umgesetzt. Ich bin nicht ganz zufrieden damit und bin für Hinweise dankbar.

Die zweite Beziehung wird etwa so umgesetzt:

EACH _User_ `MAY` _be interested in_ `ONE OR MORE` _Gender_.

EACH _Gender A_ `MAY` _be the interest of_ `ONE OR MORE` _User_.


#### User-Picture

EACH _User_  `MAY` _have as a profile picture_ `ONE AND ONLY ONE` _Picture_.

EACH _Picture_ `MAY` _be the profile picture of_ `ONE AND ONLY ONE`  _User_.


EACH _User_ `MAY` _own_ `ONE OR MORE` _Picture_.

EACH _Picture_ `MUST`  _be owned by_ `ONE AND ONLY ONE` _User_.


#### User-User

EACH _User_ `MAY` _be a friend of_ `ONE OR MORE` _User_.

EACH _User_ `MAY` _be a friend of_ `ONE OR MORE` _User_.


Aus den ERDish-Sätzen ergeben sich v.a. Optionalitäten, die im ER-Diagramm so nicht deutlich waren. Ein angepasstes ER-Diagramm mit Optionalität und ergänzt um die Ausgangsdaten (Ursprung) sieht etwa so aus:

![ER-Diagramm mit Ursprungstabelle und eingetragenen Optionalitäten](beispielloesung/images/erd-extended-optional.png)

### Logisches Datenmodell: Welche Daten befinden sich in der Excel-Datei

Zur Umwandlung in das logische Modell muss zunächst das Datenbankmodell ausgewählt werden.
In unserem Fall soll es das Relationenmodell sein, daher  müssen die [Transformationsregeln des Relationenmodells](https://oer-informatik.de/relationenmodell) angewendet werden:

- Aus allen Entitätstypen werden Relationen gebildet.

- In jeder Relation wird geprüft, ob es natürliche Schlüsselattribute (oder Attributgruppen) gibt, die einen Datensatz eindeutig beschreiben. Wo dies nicht der Fall ist werden künstliche Primärschlüssel (_artificial primary keys_) vergeben. Diese werden durch den Suffix `_id` gekennzeichnet.

- Die Relationen werden entgegen der Konvention nicht kleingeschrieben und im Plural beschrieben, sondern nach _ungarischer Notation_ mit vorangestelltem `tbl` (für _table_) und englischen Bezeichnungen benannt.

- 1:n bzw. 1:nc Beziehungen werden über Schlüsselattribute auf der "n"-Seite aufgelöst. Wenn keine Optionalität vorliegt (also bei 1:n) wird zudem ein `NOT NULL`-Constraint notiert.

- m:n wird über eine Zwischentabelle realisiert. Im Fall des assoziativen Relationshiptypen mit einem Attribut in der Zwischentabelle. Mit Optionalität wird analog zu oben mit `NOT NULL` verfahren.


![Relationenmodell auf Basis des ERD mit Constraints und Verknüpfungsrelationen](beispielloesung/plantuml/rm_beispielloesung.png)

### Erstellung des Physisches Modells: Die SQL-DDL-Befehle

#### Auswahl der Datentypen

Durch die Modellierung wurden bereits eine Reihe von Entscheidungen getroffen, die jetzt nur in SQL-DDL-Code umgesetzt werden müssen. 
Noch geklärt werden müssen die Fragen hinsichtlich der Datentypen und Constraints in der Datenbank:

Zeichenketten:

- Müssen die Daten performanceoptimiert sein (`CHAR`) oder speicherplatzoptimiert (`VARCHAR`)? _Speicherplatz ist schon länger kein Problem mehr, ich wähle daher `CHAR`._

- Bis zu welcher Länge sollen Zeichenketten gespeichert werden? _

- Welche Zeichencodierung soll genutzt werden? _Hier bietet sich UTF8 an (wir haben internationale Namensschreibweisen). UTF8 sollte ohnehin bei den meisten DBMS der Standard sein._
 
Indizes, Schlüssel, Constraints:

- Sollen diese als fortlaufende Nummer oder UUID realisiert werden? _UUID sind zwar der Weg der Wahl, in vielen DBMS aber ab Werk noch nicht möglich. Daher zunächst laufende Nr._

- Sollen die Constraints und Trigger direkt mit umgesetzt werden? 


Datumswerte:

- Welcher Datentyp soll gewählt werden?

- Welche Lokalisierung soll gewählt werden?

Constraints:

- Welche Zusicherungen müssen für einzelne Felder oder für Kombinationen aus Feldern gegeben werden? _Wird im Einzelnen je Tabelle entschieden._

- In welchen Fällen ist beim Löschen eine Löschweitergabe sinnvoll und wo nicht?  _Wird ein User oder ein Hobby gelöscht, so sollen alle verweisenden Daten gelöscht werden. Wird ein Geschlecht gelöscht, so sollen alle Daten erhalten bleiben._

Startpunkt ist eine Datenbank. In einigen Datenbankmanangementsystemen muss diese zunächst erzeugt werden, dann muss eine neue Verbindung zu der Datenbank aufgenommen werden, da alle _connections_ jeweils datenbankabhängig sind.

#### Vorbereitung der Datenbank
 
Die Datenbankverbindung und Vorbereitung des Frontends wird im zweiten Teil dieser Beispiellösung dargestellt. Hier soll es zunächst nur um die reine DDL-Struktur gehen. Zum Nachstellen der Befehle kann es trotzdem sinnvoll sein, bereits jetzt ein gestartetes DBMS zu haben.

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">


(bei _MariaDB_ z.B. [auf diesem Weg](https://oer-informatik.de/docker-install-mariadb)).

Damit das Projekt in einer eigenen Datenbankstruktur hinterlegt ist, muss diese zunächst erzeugt werden:

```sql
CREATE DATABASE letsMeetMigrationScheme;
```

In vielen Datenbankmanagementsystemen bilden _Schema_ einen Namensraum, in dem Tabellen organisiert werden können. MariaDB nutzt diese Unterteilung von Datenbanken nicht, sondern verwendet die Befehle `CREATE DATABASE` und `CREATE SCHEMA` synonym. Um in der Namensgebung parallel zu den anderen DBMS zu bleiben habe ich die Datenbank mit der Bezeichnung `...Scheme` bezeichnet.

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">


(bei _MS SQL Server_ z.B. [auf diesem Weg](https://oer-informatik.de/docker-install-mssql)).

Damit das Projekt in einer eigenen Datenbankstruktur hinterlegt ist, muss diese zunächst erzeugt werden und dann mit `USE` ausgewählt werden:

```sql
CREATE DATABASE letsmeetdb;
USE letsmeetdb;
```

Die Projektdaten sollten in einen eigenen Namensraum gelegt werden, damit wir ggf. nicht mit anderen Tabellen in Konflikt geraten. Namensräume werden in den meisten DBMS als _Schema_ organisiert. Das Standardschema bei _SQL Server_ lautet auf den Benutzernamen `dbo` in allen Abfragen würde diese Schemabezeichnung zwischen Datenbank und Tabelle stehen. Da diese Namensgebung dafür sorgen würde, dass keinerlei Kompatibiltät mit den SQL-Befehlen der anderen DBMS besteht, benennen wir das Schema um und nennen es so, wie es auch in den anderen DBMS genannt wurde:

```sql
CREATE SCHEMA letsMeetMigrationScheme;
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">


(bei _PostgreSQL_ z.B. [auf diesem Weg](https://oer-informatik.de/docker-install-postgres)).

Damit das Projekt in einer eigenen Datenbankstruktur hinterlegt ist, muss diese zunächst erzeugt werden:

```sql
CREATE DATABASE letsmeetdb;
```

Bei _PostgreSQL_ verbindet sich ein Frontend bei einer Datenbankverbindung immer direkt mit einer Datenbank - hier müsste daher eine neue Verbindung zur neuen DB aufgebaut werden, um fortzufahren.


Die Projektdaten sollten in einen eigenen Namensraum gelegt werden, damit wir ggf. nicht mit anderen Tabellen in Konflikt geraten. Namensräume werden in den meisten DBMS als _Schema_ organisiert:

```sql
CREATE SCHEMA letsMeetMigrationScheme;
```

</span>


### Erstellen der Tabellen

Die Tabellen müssen in der richtigen Reihenfolge erstellt und gelöscht werden: Alle Tabellen, die keinerlei Fremdschlüssel enthalten können unabhängig erstellt werden (`tblGender`, `tblUrsprung`, `tblHobby`). `tblUser` muss nach `tblGender` erstellt werden, `tblProfilePicture` zuletzt, alle übrigen nach `tblUser`. Alternativ können die Tabellen zunächst erstellt werden und später die entsprechenden _Foreign Key Constraints_ per `ALTER TABLE` nachgereicht werden.

![Abhängigkeit zwischen den Tabellen als Gantt-Diagramm](beispielloesung/plantuml/tabellenreihenfolge.png)

Wenn mal etwas schiefgeht, möchte ich die Tabellen schnell löschen und habe die entsprechenden Befehle immer direkt mit verzeichnet. Aber beim Löschen gilt genau das Gegenteil: Ich kann `tblGender`, `tblUser`, `tblHobby` und `tblPicture` erst löschen, nachdem es keine Fremdschlüsselbeziehungen mehr gibt. Die Lösch-Befehle muss ich also in umgekehrter Reihenfolge ausführen.

Die Ursprungstabelle ist noch am einfachsten: es wird zunächst alles als Text eingelesen - ich wähle den performanceoptimierten `CHAR` als Datentyp, die Länge wähle ich zunächst per Bauchgefühl und muss ggf. später noch anpassen. Mit der UserID wird ein Schlüssel mit laufender Nummer erzeugt.

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

In MariaDB/MySQL werden `ID`s per `INT PRIMARY KEY AUTO_INCREMENT` erstellt:

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUrsprung;
CREATE TABLE letsMeetMigrationScheme.tblUrsprung (
    user_id  INT PRIMARY KEY AUTO_INCREMENT,
    namen CHAR(64),      /* Vorname und Nachname nicht über 32 */
    adresse CHAR(128),   /* Ort und Straße nicht über 64 */
    telefon CHAR(16),    /* bei einer Nr. nicht über 16 */
    hobbytext CHAR(255), /* 255 better be safe than sorry... */
    email CHAR(64),      /* realistisch nicht über 64 */
    geschlecht CHAR(4),  /* m,w,nb => max 4 + Puffer */
    interesse CHAR(8),   /* m,w,nb => max 4 + Puffer */
    geburtstag CHAR(10)  /* DD.MM.YYYY => max 10 Zeichen */
);

DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby;
CREATE TABLE letsMeetMigrationScheme.tblHobby (
   hobby_id INT PRIMARY KEY AUTO_INCREMENT,
   hobbyname CHAR(127) NOT NULL UNIQUE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

In `SQL-Server` wird für fortlaufende IDs die Kurzform `IDENTITY(1,1)` verwendet:

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUrsprung;
CREATE TABLE letsMeetMigrationScheme.tblUrsprung (
    user_id  int IDENTITY(1,1) PRIMARY KEY,
    namen char(64),      /* Vorname und Nachname nicht über 32 */
    adresse char(128),   /* Ort und Straße nicht über 64 */
    telefon char(16),    /* bei einer Nr. nicht über 16 */
    hobbytext char(255), /* 255 better be safe than sorry... */
    email char(64),      /* realistisch nicht über 64 */
    geschlecht char(4),  /* m,w,nb => max 4 + Puffer */
    interesse char(8),   /* m,w,nb => max 4 + Puffer */
    geburtstag char(10)  /* DD.MM.YYYY => max 10 Zeichen */
);

DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby;
CREATE TABLE letsMeetMigrationScheme.tblHobby (
   hobby_id int IDENTITY(1,1) PRIMARY KEY,
   hobbyname CHAR(127) NOT NULL UNIQUE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

In `postgres` wird für fortlaufende IDs die Kurzform `SERIAL` verwendet:

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUrsprung;
CREATE TABLE letsMeetMigrationScheme.tblUrsprung (
    user_id SERIAL PRIMARY KEY,
    namen CHAR(64),      /* Vorname und Nachname nicht über 32 */
    adresse CHAR(128),   /* Ort und Straße nicht über 64 */
    telefon CHAR(16),    /* bei einer Nr. nicht über 16 */
    hobbytext CHAR(255), /* 255 better be safe than sorry... */
    email CHAR(64),      /* realistisch nicht über 64 */
    geschlecht CHAR(4),  /* m,w,nb => max 4 + Puffer */
    interesse CHAR(8),   /* m,w,nb => max 4 + Puffer */
    geburtstag CHAR(10)  /* DD.MM.YYYY => max 10 Zeichen */
);
```

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby;
CREATE TABLE letsMeetMigrationScheme.tblHobby (
   hobby_id SERIAL PRIMARY KEY,
   hobbyname CHAR(127) NOT NULL UNIQUE
);
```

</span>

`tblGender` umfasst lediglich die Werte `m`, `w`, `nb`, da ich noch nicht 100% überzeugt bin, dass das dem Anwendungsfall gerecht wird, gönne ich mir hier 8 Zeichen.

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGender;
CREATE TABLE letsMeetMigrationScheme.tblGender (
      gender CHAR(8) PRIMARY KEY
);
```

`tblUser` nutzt im Wesentlichen die getroffenen Datentypenentscheidungen von Ursprung - die Größen der vormals zusammengefassten Attribute wurden halbiert. Birthday wird als reines Datumsfeld gespeichert. Mit `gender` ist es erstmals nötig, einen Fremdschlüssel zu verzeichnen (Constraint), entsprechend hat das Attribut den gleichen Datentyp wie oben. 


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUser;
CREATE TABLE letsMeetMigrationScheme.tblUser (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    firstname CHAR(32) NOT NULL,
    lastname CHAR(32) NOT NULL,
    telephone CHAR(16),
    street CHAR(64),
	  street_no CHAR(16),
    postcode CHAR(16),
    city CHAR(64),
    country CHAR(64),
    email CHAR(64),
    gender CHAR(8),
    birthday DATE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUser;
CREATE TABLE letsMeetMigrationScheme.tblUser (
    user_id int IDENTITY(1,1) PRIMARY KEY,
    firstname CHAR(32) NOT NULL,
    lastname CHAR(32) NOT NULL,
    telephone CHAR(16),
    street CHAR(64),
	  street_no CHAR(16),
    postcode CHAR(16),
    city CHAR(64),
    country CHAR(64),
    email CHAR(64),
    gender CHAR(8),
    birthday DATE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUser;
CREATE TABLE letsMeetMigrationScheme.tblUser (
    user_id SERIAL PRIMARY KEY,
    firstname CHAR(32) NOT NULL,
    lastname CHAR(32) NOT NULL,
    telephone CHAR(16),
    street CHAR(64),
    street_no CHAR(16),
    postcode CHAR(16),
    city CHAR(64),
    country CHAR(64),
    email CHAR(64),
    gender CHAR(8),
    birthday DATE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);
```

</span>

Die Einträge in der Tabelle `tblGenderInterest` sind nur für bestehende Userkonten relevant. Daher wurde beim _Foreign Key Constraint_ für die `user_id` eine Löschweitergabe vorgesehen (`ON DELETE CASCADE`). Selbiges wäre auch beim Löschen des Geschlechts denkbar, diese Information würde ich aber sicherheitshalber behalten. Der Primärschlüssel ist hier ein zusammengesetzter, der gesondert benannt werden muss:


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGenderInterest;
CREATE TABLE letsMeetMigrationScheme.tblGenderInterest(
    gender CHAR(8),
    user_id INT,
    PRIMARY KEY (gender, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGenderInterest;
CREATE TABLE letsMeetMigrationScheme.tblGenderInterest(
    gender char(8),
    user_id int,
    PRIMARY KEY (gender, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGenderInterest;
CREATE TABLE letsMeetMigrationScheme.tblGenderInterest(
    gender CHAR(8),
    user_id INTEGER,
    PRIMARY KEY (gender, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);
```

</span>

Bei `tblHobby2User` und `tblUserLikesPeopleWithHobby` gelten dieselben Überlegungen: Löschweitergabe, zusammengesetzter Primärschlüssel:


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby2User;
CREATE TABLE letsMeetMigrationScheme.tblHobby2User (
    user_id INT,
    hobby_id INT,
    priority INT,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUserLikesPeopleWithHobby;
CREATE TABLE letsMeetMigrationScheme.tblUserLikesPeopleWithHobby (
    user_id INT,
    hobby_id INT,
    priority INT,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby2User;
CREATE TABLE letsMeetMigrationScheme.tblHobby2User (
    user_id int,
    hobby_id int,
    priority int,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUserLikesPeopleWithHobby;
CREATE TABLE letsMeetMigrationScheme.tblUserLikesPeopleWithHobby (
    user_id int,
    hobby_id int,
    priority int,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby2User;
CREATE TABLE letsMeetMigrationScheme.tblHobby2User (
    user_id INTEGER,
    hobby_id INTEGER,
    priority INTEGER,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);
```

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUserLikesPeopleWithHobby;
CREATE TABLE letsMeetMigrationScheme.tblUserLikesPeopleWithHobby (
    user_id INTEGER,
    hobby_id INTEGER,
    priority INTEGER,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);
```

</span>

Bleiben noch die Tabellen der erweiterten Aufgabenstellung: die Tabelle `tblFriendship` modelliert die selbstreferenzierende Beziehung, entsprechend verweisen zwei _Foreign Keys_ auf `user_id`. Wie bei allen Userkontenbezügen wird auch hier eine Löschweitergabe vorgesehen:


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblFriendship;
CREATE TABLE letsMeetMigrationScheme.tblFriendship(
    user_id INT,
    friend_user_id INT,
    PRIMARY KEY (friend_user_id, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (friend_user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblFriendship;
CREATE TABLE letsMeetMigrationScheme.tblFriendship(
    user_id int,
    friend_user_id int,
    PRIMARY KEY (friend_user_id, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE NO ACTION,
    FOREIGN KEY (friend_user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE NO ACTION
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblFriendship;
CREATE TABLE letsMeetMigrationScheme.tblFriendship(
    user_id INTEGER,
    friend_user_id INTEGER,
    PRIMARY KEY (friend_user_id, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (friend_user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);
```

</span>

Bei `tblPicture` ist die Besonderheit, dass mit einem _Check-Constraint_ sichergestellt wird, dass entweder eine URL oder ein Bild als Datei vorhanden ist. Der Dateityp von `pictureData` ist zum Speichern von Binärdateien vorgesehen.


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblPicture;
CREATE TABLE letsMeetMigrationScheme.tblPicture (
  picture_id INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT,
  picture_url text,
  pictureData BLOB,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  CHECK ((picture_url IS NOT NULL) OR (pictureData IS NOT NULL))
 );
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblPicture;
CREATE TABLE letsMeetMigrationScheme.tblPicture (
  picture_id int IDENTITY(1,1) PRIMARY KEY,
  user_id int,
  picture_url nvarchar,
  pictureData binary,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  CHECK ((picture_url IS NOT NULL) OR (pictureData IS NOT NULL))
 );
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblPicture;
CREATE TABLE letsMeetMigrationScheme.tblPicture (
  picture_id SERIAL PRIMARY KEY,
  user_id INTEGER,
  picture_url text,
  pictureData bytea,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  CHECK ((picture_url IS NOT NULL) OR (pictureData IS NOT NULL))
 );
```

</span>

Bleibt schließlich noch die Tabelle `tblProfilePicture`. Damit diese eine 1:1 Beziehung zwischen `tblUser` und `tblPicture` abbildet, muss ein _UNIQUE-Constraint_ gesetzt werden.


<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MySQL/MariaDB</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>


<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblProfilePicture;
CREATE TABLE letsMeetMigrationScheme.tblProfilePicture(
  user_id INT PRIMARY KEY,
  picture_id INT NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  FOREIGN KEY (picture_id) REFERENCES letsMeetMigrationScheme.tblPicture(picture_id) ON DELETE CASCADE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblProfilePicture;
CREATE TABLE letsMeetMigrationScheme.tblProfilePicture(
  user_id int PRIMARY KEY,
  picture_id int NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE NO ACTION,
  FOREIGN KEY (picture_id) REFERENCES letsMeetMigrationScheme.tblPicture(picture_id) ON DELETE CASCADE
);
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblProfilePicture;
CREATE TABLE letsMeetMigrationScheme.tblProfilePicture(
  user_id INTEGER PRIMARY KEY,
  picture_id INTEGER NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  FOREIGN KEY (picture_id) REFERENCES letsMeetMigrationScheme.tblPicture(picture_id) ON DELETE CASCADE
);
```

</span>

### Testen der SQL-DDL-Befehle

Aus der Modellierung ergeben sich bereits eine Reihe von Testfällen, die erst mit laufendem Datenbanksystem ausgeführt werden können. Damit sie nicht in Vergessenheit geraten, sollten sie bereits notiert werden:

|Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte | Erwartetes<br/>Resultat<br/>gemäß<br/>Spezifikation | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
--- | --- | --- |--- | --- | --- |--- | ---
|DDL-1 | Datentypen: die gewählten Datenbreiten sind ausreichend  | |Import der größten vorgegebenen Daten| Fehlerfreie Speicherung der Werte | - || |
|DDL-2 | Laufende Nummern werden als Index vergeben |  || | - | | |	
|DDL-3 | Check-Constraint in `tblProfilePicture` |  |Weder URL noch Datei wird übergeben| Speicherung nicht möglich | - |  | |
|DDL-4 | Löschweitergabe bei `tblUser` |  | | - |  | |
|DDL-5 | Keine Löschweitergabe bei `tblGender` |  | | - |  | |
|DDL-6 | Speichern von Einträgen mit `NOT NULL` Constraint-Verstoß nicht möglich |  | | - |  | |

### Fazit der Modellierung

Damit wäre die Struktur für die Ausgangstabelle (von Excel nach `tblUrsprung`) und das Zielsystem modelliert und per SQL-Befehl definiert. Ein fertiges SQL-Skript findet sich [hier für MariaDB](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql), [hier für MS SQL Server](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql) und [hier für postgres](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql).

Beispiele für den [Import (Teil 2)](https://oer-informatik.de/lets_meet-beispielloesung_import) und die Normalisierung im Bestand werden in zwei weiteren Artikeln beschrieben. 



### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)


