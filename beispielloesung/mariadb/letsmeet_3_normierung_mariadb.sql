/* Struktur anpassen für Vorname / Nachname */

ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD firstname CHAR(32);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD lastname CHAR(32);

/* Daten anpassen für Vorname/Nachname */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 lastname =TRIM(SUBSTRING( namen, 1, LOCATE(', ', namen)-1)),
 firstname =TRIM(SUBSTRING( namen, LOCATE(', ', namen )+2 ));


/* Struktur vorbereiten für Adresse */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode_city CHAR(128);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_nr CHAR(80);

/* Daten vorbereiten für Adresse */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street_nr = TRIM(SUBSTRING(adresse,1,LOCATE(', ', adresse)-1)),
 postcode_city = TRIM(SUBSTRING( adresse FROM (LOCATE(', ', adresse)+2) )) ;

/* Struktur anpassen für Straße / Hausnummer */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_no CHAR(16);

/* Daten anpassen für Straße / Hausnummer */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street = TRIM(SUBSTRING(street_nr, REGEXP_INSTR(street_nr, '[^0-9]+'), REGEXP_INSTR(street_nr, '[0-9]+.*')-1) ),
 street_no = TRIM(SUBSTRING(street_nr, REGEXP_INSTR(street_nr, '[0-9]+.*')));

/* Struktur anpassen für PLZ / Ort */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD city CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode CHAR(16);

/* Daten anpassen für PLZ / Ort */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 postcode = TRIM(SUBSTRING(postcode_city, 1, LOCATE(', ', postcode_city)-1 )),
 city = TRIM(SUBSTRING( postcode_city, LOCATE(', ', postcode_city)+2) );

/* Struktur anpassen für Hobbys */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobbyrest CHAR(255);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5 CHAR(132);

/* Daten anpassen für Hobbys */

/* Daten vorbereiten für Hobby1 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1 = TRIM(SUBSTRING(hobbytext, 1, LOCATE(';', hobbytext)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbytext,  (LOCATE(';', hobbytext)+1) ))
WHERE LOCATE(';', hobbytext) >0;

/* Daten vorbereiten für Hobby2 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;

/* Daten vorbereiten für Hobby3 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;

/* Daten vorbereiten für Hobby4 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;

/* Daten vorbereiten für Hobby5 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5 = TRIM(SUBSTRING(hobbyrest, 1, LOCATE(';', hobbyrest)-1 )),
hobbyrest = TRIM(SUBSTRING(hobbyrest,  (LOCATE(';', hobbyrest)+1) ))
WHERE LOCATE(';', hobbyrest) >0;

/* Struktur vorbereiten für die Hobby-Prioritäten */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5_prio INTEGER;

/* Hobbyname und Hobby-Prioritäten aufteilen*/
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1_prio = CAST( TRIM(TRIM('%' FROM SUBSTRING(hobby1,  (LOCATE(' %', hobby1)+2)) )) AS UNSIGNED INTEGER),
hobby1 = TRIM(SUBSTRING(hobby1, 1, LOCATE(' %', hobby1) ))
WHERE position (' %' IN hobby1 ) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby2,  (LOCATE(' %', hobby2)+2))) )AS UNSIGNED INTEGER),
hobby2 = TRIM(SUBSTRING(hobby2, 1, LOCATE(' %', hobby2) ))
WHERE LOCATE(' %', hobby2) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby3,  (LOCATE(' %', hobby3)+2))) )AS UNSIGNED INTEGER),
hobby3 = TRIM(SUBSTRING(hobby3, 1, LOCATE(' %', hobby3) ))
WHERE LOCATE(' %', hobby3) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby4,  (LOCATE(' %', hobby4)+2))) )AS UNSIGNED INTEGER),
hobby4 = TRIM(SUBSTRING(hobby4, 1, LOCATE(' %', hobby4) ))
WHERE LOCATE(' %', hobby4) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5_prio = CAST(TRIM(TRIM( '%' FROM SUBSTRING(hobby5,  (LOCATE(' %', hobby5)+2))) )AS UNSIGNED INTEGER),
hobby5 = TRIM(SUBSTRING(hobby5, 1, LOCATE(' %', hobby5) ))
WHERE LOCATE(' %', hobby5) >0;

/* Hobbys in neuer Tabelle speichern */
insert INTO letsMeetMigrationScheme.tblHobby(hobbyname)
(SELECT DISTINCT hobby FROM (
(SELECT DISTINCT hobby1 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby2 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby3 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby4 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby5 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
 ) AS ursprung
WHERE hobby IS NOT NULL
ORDER BY hobby);


/* Geschlechter aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblGender(gender)
SELECT
tblUrsprung.geschlecht AS gender
FROM letsMeetMigrationScheme.tblUrsprung
GROUP by tblUrsprung.geschlecht;

/* User aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblUser
    (user_id,firstname,lastname, telephone, street, street_no, postcode, city, country, email, gender, birthday)
    SELECT
        tblUrsprung.user_id AS user_id,
        tblUrsprung.firstname AS firstname,
        tblUrsprung.lastname AS lastname,
        tblUrsprung.telefon AS telephone,
        tblUrsprung.street AS street,
        tblUrsprung.street_no  AS street_no,
        tblUrsprung.postcode  AS postcode,
        tblUrsprung.city AS city,
        'Deutschland' AS country,
        tblUrsprung.email AS email,
        tblUrsprung.geschlecht  AS gender,
        STR_TO_DATE(tblUrsprung.geburtstag,'%d.%m.%Y') AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung;

/* User-Hobby Zuweisungen aus tblUrsprung auslesen und schreiben */

INSERT INTO letsMeetMigrationScheme.tblHobby2User
 (hobby_id,user_id,priority)
 SELECT hobby_id, user_id, prio 
    FROM (
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby1_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby1)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby2_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby2)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby3_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby3)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby4_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby4)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby5_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby5)
    ) AS T
WHERE T.prio IS NOT NULL;

/* GenderInterest aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblGenderInterest
(gender, user_id)
SELECT  letsMeetMigrationScheme.tblGender.gender, letsMeetMigrationScheme.tblUrsprung.user_id
FROM letsMeetMigrationScheme.tblUrsprung
LEFT JOIN letsMeetMigrationScheme.tblGender
ON (letsMeetMigrationScheme.tblUrsprung.interesse LIKE CONCAT('%', letsMeetMigrationScheme.tblGender.gender, '%'));
