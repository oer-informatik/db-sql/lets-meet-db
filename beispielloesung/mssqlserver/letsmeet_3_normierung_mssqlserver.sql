/* Struktur anpassen für Vorname / Nachname */

ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD firstname char(32);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD lastname char(32);

/* Daten anpassen für Vorname/Nachname */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 lastname =TRIM(LEFT( namen, PATINDEX('%, %', namen)-1)),
 firstname =TRIM(RIGHT(RTRIM(namen), LEN( namen )-PATINDEX('%, %', namen )));


/* Struktur vorbereiten für Adresse */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode_city char(128);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_nr char(80);

/* Daten vorbereiten für Adresse */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street_nr = TRIM(LEFT(adresse, PATINDEX('%, %', adresse)-1)),
 postcode_city = TRIM(RIGHT(RTRIM(adresse), LEN(adresse) - PATINDEX('%, %', adresse)) ) ;

/* Struktur anpassen für Straße / Hausnummer */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street char(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_no char(16);

/* Daten anpassen für Straße / Hausnummer */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street = TRIM(LEFT(street_nr, PATINDEX('%[0-9]%', street_nr)-1)),
 street_no = TRIM(RIGHT(RTRIM(street_nr), LEN(street_nr)-PATINDEX('%[0-9]%', street_nr)+2));

/* Struktur anpassen für PLZ / Ort */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD city char(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode char(16);

/* Daten anpassen für PLZ / Ort */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 postcode = TRIM(LEFT(postcode_city, PATINDEX('%, %', postcode_city)-1 )),
 city = TRIM(RIGHT(RTRIM(postcode_city), LEN(postcode_city) - PATINDEX('%, %', postcode_city)) );

/* Struktur anpassen für Hobbys */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobbyrest char(255);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4 char(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5 char(132);

/* Daten anpassen für Hobbys */

/* Daten vorbereiten für Hobby1 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1 = TRIM(LEFT(hobbytext, PATINDEX('%;%', hobbytext)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbytext),  (LEN(hobbytext)-PATINDEX('%;%', hobbytext)) ))
WHERE PATINDEX('%;%', hobbytext) >0;

/* Daten vorbereiten für Hobby2 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;

/* Daten vorbereiten für Hobby3 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;

/* Daten vorbereiten für Hobby4 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;

/* Daten vorbereiten für Hobby5 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5 = TRIM(LEFT(hobbyrest, PATINDEX('%;%', hobbyrest)-1 )),
hobbyrest = TRIM(RIGHT(RTRIM(hobbyrest),  (LEN(hobbyrest )-PATINDEX('%;%', hobbyrest)) ))
WHERE PATINDEX('%;%', hobbyrest) >0;

/* Struktur vorbereiten für die Hobby-Prioritäten */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4_prio int;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5_prio int;

/* Hobbyname und Hobby-Prioritäten aufteilen*/
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby1),  LEN(hobby1) -(PATINDEX('% [%]%', hobby1))) )) AS int),
hobby1 = TRIM(LEFT(hobby1, PATINDEX('% [%]%', hobby1) ))
WHERE PATINDEX('% [%]%', hobby1) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby2),  LEN(hobby2) -(PATINDEX('% [%]%', hobby2))) )) AS int),
hobby2 = TRIM(LEFT(hobby2, PATINDEX('% [%]%', hobby2) ))
WHERE PATINDEX('% [%]%', hobby2) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby3),  LEN(hobby3) -(PATINDEX('% [%]%', hobby3))) )) AS int),
hobby3 = TRIM(LEFT(hobby3, PATINDEX('% [%]%', hobby3) ))
WHERE PATINDEX('% [%]%', hobby3) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby4),  LEN(hobby4) -(PATINDEX('% [%]%', hobby4))) )) AS int),
hobby4 = TRIM(LEFT(hobby4, PATINDEX('% [%]%', hobby4) ))
WHERE PATINDEX('% [%]%', hobby4) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5_prio = CAST(TRIM(TRIM('%' FROM RIGHT(RTRIM(hobby5),  LEN(hobby5) -(PATINDEX('% [%]%', hobby5))) )) AS int),
hobby5 = TRIM(LEFT(hobby5, PATINDEX('% [%]%', hobby5) ))
WHERE PATINDEX('% [%]%', hobby5) >0;

/* Hobbys in neuer Tabelle speichern */
insert INTO letsMeetMigrationScheme.tblHobby(hobbyname)
(SELECT DISTINCT hobby FROM (
(SELECT DISTINCT hobby1 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby2 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby3 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby4 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby5 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
 ) AS ursprung
WHERE hobby IS NOT NULL
);


/* Geschlechter aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblGender(gender)
SELECT
tblUrsprung.geschlecht AS gender
FROM letsMeetMigrationScheme.tblUrsprung
GROUP by tblUrsprung.geschlecht;

/* User aus tblUrsprung auslesen und schreiben */
SET IDENTITY_INSERT letsMeetMigrationScheme.tblUser ON; 
GO

INSERT INTO letsMeetMigrationScheme.tblUser
    (user_id,firstname,lastname, telephone, street, street_no, postcode, city, country, email, gender, birthday)
    SELECT
    tblUrsprung.user_id AS user_id,
    tblUrsprung.firstname AS firstname,
    tblUrsprung.lastname AS lastname,
    tblUrsprung.telefon AS telephone,
    tblUrsprung.street AS street,
    tblUrsprung.street_no  AS street_no,
    tblUrsprung.postcode  AS postcode,
    tblUrsprung.city AS city,
    'Deutschland' AS country,
    tblUrsprung.email AS email,
    tblUrsprung.geschlecht  AS gender,
    parse(tblUrsprung.geburtstag as date using 'de-DE' ) AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung;
GO

SET IDENTITY_INSERT letsMeetMigrationScheme.tblUser OFF;
GO

/* User-Hobby Zuweisungen aus tblUrsprung auslesen und schreiben */
	INSERT INTO letsMeetMigrationScheme.tblHobby2User
    (hobby_id,user_id,priority)
INSERT INTO letsMeetMigrationScheme.tblHobby2User
 (hobby_id,user_id,priority)
 SELECT hobby_id, user_id, prio 
    FROM (
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby1_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby1)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby2_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby2)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby3_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby3)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby4_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby4)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby5_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby5)
    ) AS T
WHERE T.prio IS NOT NULL;

/* GenderInterest aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblGenderInterest
(gender, user_id)
SELECT  letsMeetMigrationScheme.tblGender.gender, letsMeetMigrationScheme.tblUrsprung.user_id
FROM letsMeetMigrationScheme.tblUrsprung
LEFT JOIN letsMeetMigrationScheme.tblGender
ON (letsMeetMigrationScheme.tblUrsprung.interesse LIKE '%'+TRIM(letsMeetMigrationScheme.tblGender.gender)+'%');
