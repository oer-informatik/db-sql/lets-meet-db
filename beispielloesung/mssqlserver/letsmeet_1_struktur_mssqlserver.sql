IF EXISTS(SELECT * FROM sys.databases WHERE name = 'letsmeetdb')
  BEGIN
  DROP DATABASE letsmeetdb
  END

GO

CREATE DATABASE letsmeetdb

GO

USE letsmeetdb;

IF NOT EXISTS ( SELECT *

 FROM sys.schemas

 WHERE name = N'letsMeetMigrationScheme' )

 EXEC('CREATE SCHEMA letsMeetMigrationScheme AUTHORIZATION [dbo]');

GO

/* Zuerst Tabellen mit Fremdschlüsseln löschen, dann die referenzierten */
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblFriendship;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGenderInterest;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblProfilePicture;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblPicture;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUserLikesPeopleWithHobby;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby2User;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUser;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGender;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUrsprung;

GO

CREATE TABLE letsMeetMigrationScheme.tblUrsprung (
    user_id  int IDENTITY(1,1) PRIMARY KEY,
    namen char(64),      /* Vorname und Nachname nicht über 32 */
    adresse char(128),   /* Ort und Straße nicht über 64 */
    telefon char(16),    /* bei einer Nr. nicht über 16 */
    hobbytext char(255), /* 255 better be safe than sorry... */
    email char(64),      /* realistisch nicht über 64 */
    geschlecht char(4),  /* m,w,nb => max 4 + Puffer */
    interesse char(8),   /* m,w,nb => max 4 + Puffer */
    geburtstag char(10)  /* DD.MM.YYYY => max 10 Zeichen */
);

CREATE TABLE letsMeetMigrationScheme.tblHobby (
   hobby_id int IDENTITY(1,1) PRIMARY KEY,
   hobbyname char(127) NOT NULL UNIQUE
);

CREATE TABLE letsMeetMigrationScheme.tblGender (
      gender char(8) PRIMARY KEY
);

CREATE TABLE letsMeetMigrationScheme.tblUser (
    user_id int IDENTITY(1,1) PRIMARY KEY,
    firstname char(32) NOT NULL,
    lastname char(32) NOT NULL,
    telephone char(16),
    street char(64),
	street_no char(16),
    postcode char(16),
    city char(64),
    country char(64),
    email char(64),
    gender char(8),
    birthday DATE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);


CREATE TABLE letsMeetMigrationScheme.tblHobby2User (
    user_id int,
    hobby_id int,
    priority int,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);

CREATE TABLE letsMeetMigrationScheme.tblUserLikesPeopleWithHobby (
    user_id int,
    hobby_id int,
    priority int,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);


CREATE TABLE letsMeetMigrationScheme.tblGenderInterest(
    gender char(8),
    user_id int,
    PRIMARY KEY (gender, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);


CREATE TABLE letsMeetMigrationScheme.tblFriendship(
    user_id int,
    friend_user_id int,
    PRIMARY KEY (friend_user_id, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE NO ACTION,
    FOREIGN KEY (friend_user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE NO ACTION
);

CREATE TABLE letsMeetMigrationScheme.tblPicture (
  picture_id int IDENTITY(1,1) PRIMARY KEY,
  user_id int,
  picture_url nvarchar,
  pictureData binary,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  CHECK ((picture_url IS NOT NULL) OR (pictureData IS NOT NULL))
 );

CREATE TABLE letsMeetMigrationScheme.tblProfilePicture(
  user_id int PRIMARY KEY,
  picture_id int NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE NO ACTION,
  FOREIGN KEY (picture_id) REFERENCES letsMeetMigrationScheme.tblPicture(picture_id) ON DELETE CASCADE
);

GO