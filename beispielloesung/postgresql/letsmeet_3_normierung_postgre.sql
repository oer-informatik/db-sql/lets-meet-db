/* Struktur anpassen für Vorname / Nachname */

ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD firstname CHAR(32);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD lastname CHAR(32);

/* Daten anpassen für Vorname/Nachname */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 lastname =trim(substring ( namen FROM 0 FOR position (', ' IN namen ) ) ),
 firstname =trim(substring ( namen FROM (position(', ' IN namen )+2) ));


/* Struktur vorbereiten für Adresse */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode_city CHAR(128);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_nr CHAR(80);

/* Daten vorbereiten für Adresse */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street_nr = trim(substring ( adresse FROM 0 FOR position (', ' IN adresse ) )),
 postcode_city = trim(substring ( adresse FROM (position(', ' IN adresse )+2) )) ;

/* Struktur anpassen für Straße / Hausnummer */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD street_no CHAR(16);

/* Daten anpassen für Straße / Hausnummer */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 street = trim(substring (street_nr FROM '[^0-9]+' )),
 street_no = trim(substring (street_nr FROM '[0-9]+.*' ));

/* Struktur anpassen für PLZ / Ort */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD city CHAR(64);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD postcode CHAR(16);

/* Daten anpassen für PLZ / Ort */
UPDATE letsMeetMigrationScheme.tblUrsprung SET
 postcode = trim(substring (postcode_city FROM 0 FOR position (', ' IN postcode_city ) )),
 city = trim(substring ( postcode_city FROM (position(', ' IN postcode_city )+2) ));

/* Struktur anpassen für Hobbys */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobbyrest CHAR(255);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4 CHAR(132);
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5 CHAR(132);

/* Daten anpassen für Hobbys */

/* Daten vorbereiten für Hobby1 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1 = trim(substring (hobbytext FROM 0 FOR position (';' IN hobbytext ) )),
hobbyrest = trim(substring (hobbytext FROM (position(';' IN hobbytext )+1) ))
WHERE position (';' IN hobbytext ) >0;

/* Daten vorbereiten für Hobby2 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2 = trim(substring (hobbyrest FROM 0 FOR position (';' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(';' IN hobbyrest )+1) ))
WHERE position (';' IN hobbyrest ) >0;

/* Daten vorbereiten für Hobby3 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3 = trim(substring (hobbyrest FROM 0 FOR position (';' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(';' IN hobbyrest )+1) ))
WHERE position (';' IN hobbyrest ) >0;

/* Daten vorbereiten für Hobby4 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4 = trim(substring (hobbyrest FROM 0 FOR position (';' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(';' IN hobbyrest )+1) ))
WHERE position (';' IN hobbyrest ) >0;

/* Daten vorbereiten für Hobby5 */
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5 = trim(substring (hobbyrest FROM 0 FOR position (';' IN hobbyrest ) )),
hobbyrest = trim(substring (hobbyrest FROM (position(';' IN hobbyrest )+1) ))
WHERE position (';' IN hobbyrest ) >0;

/* Struktur vorbereiten für die Hobby-Prioritäten */
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby1_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby2_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby3_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby4_prio INTEGER;
ALTER TABLE letsMeetMigrationScheme.tblUrsprung ADD hobby5_prio INTEGER;

/* Hobbyname und Hobby-Prioritäten aufteilen*/
UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby1_prio = CAST (trim(trim ( '%' FROM  substring (hobby1 FROM (position(' %' IN hobby1 )+2)  ))) AS INTEGER),
hobby1 = trim(substring (hobby1 FROM 0 FOR position (' %' IN hobby1 ) ))
WHERE position (' %' IN hobby1 ) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby2_prio = CAST(trim(trim ( '%' FROM  substring (hobby2 FROM (position(' %' IN hobby2 )+2)  ))) AS INTEGER),
hobby2 = trim(substring (hobby2 FROM 0 FOR position (' %' IN hobby2) ))
WHERE position (' %' IN hobby2 ) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby3_prio = CAST(trim(trim ( '%' FROM  substring (hobby3 FROM (position(' %' IN hobby3 )+2)  ))) AS INTEGER),
hobby3 = trim(substring (hobby3 FROM 0 FOR position (' %' IN hobby3) ))
WHERE position (' %' IN hobby3 ) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby4_prio = CAST(trim(trim ( '%' FROM  substring (hobby4 FROM (position(' %' IN hobby4 )+2)  ))) AS INTEGER),
hobby4 = trim(substring (hobby4 FROM 0 FOR position (' %' IN hobby4) ))
WHERE position (' %' IN hobby4 ) >0;


UPDATE  letsMeetMigrationScheme.tblUrsprung SET
hobby5_prio = CAST(trim(trim ( '%' FROM  substring (hobby5 FROM (position(' %' IN hobby5 )+2)  ))) AS INTEGER),
hobby5 = trim(substring (hobby5 FROM 0 FOR position (' %' IN hobby5) ))
WHERE position (' %' IN hobby5 ) >0;

/* Hobbys in neuer Tabelle speichern */
insert INTO letsMeetMigrationScheme.tblHobby(hobbyname)
(SELECT DISTINCT hobby FROM (
(SELECT DISTINCT hobby1 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby2 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby3 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby4 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
UNION
(SELECT DISTINCT hobby5 AS hobby
FROM letsMeetMigrationScheme.tblUrsprung)
 ) AS ursprung
WHERE hobby IS NOT NULL
ORDER BY hobby);


/* Geschlechter aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblGender(gender)
SELECT
tblUrsprung.geschlecht AS gender
FROM letsMeetMigrationScheme.tblUrsprung
GROUP by tblUrsprung.geschlecht;

/* User aus tblUrsprung auslesen und schreiben */
INSERT INTO letsMeetMigrationScheme.tblUser
    (user_id,firstname,lastname, telephone, street, street_no, postcode, city, country, email, gender, birthday)
    SELECT
        tblUrsprung.user_id AS user_id,
        tblUrsprung.firstname AS firstname,
        tblUrsprung.lastname AS lastname,
        tblUrsprung.telefon AS telephone,
        tblUrsprung.street AS street,
        tblUrsprung.street_no  AS street_no,
        tblUrsprung.postcode  AS postcode,
        tblUrsprung.city AS city,
        'Deutschland' AS country,
        tblUrsprung.email AS email,
        tblUrsprung.geschlecht  AS gender,
        TO_DATE(tblUrsprung.geburtstag,'DD.MM.YYYY') AS birthday
    FROM letsMeetMigrationScheme.tblUrsprung;
	
/* User-Hobby Zuweisungen aus tblUrsprung auslesen und schreiben */	
INSERT INTO letsMeetMigrationScheme.tblHobby2User
 (hobby_id,user_id,priority)
 SELECT hobby_id, user_id, prio 
    FROM (
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby1_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby1)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby2_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby2)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby3_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby3)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby4_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby4)
    UNION
        (SELECT letsMeetMigrationScheme.tblHobby.hobby_id AS hobby_id,
            letsMeetMigrationScheme.tblUrsprung.user_id AS user_id,
            letsMeetMigrationScheme.tblUrsprung.hobby5_prio AS prio
        FROM letsMeetMigrationScheme.tblUrsprung 
        LEFT JOIN letsMeetMigrationScheme.tblHobby
            ON letsMeetMigrationScheme.tblHobby.hobbyname = letsMeetMigrationScheme.tblUrsprung.hobby5)
    ) AS T
WHERE T.prio IS NOT NULL;

/* GenderInterest aus tblUrsprung auslesen und schreiben */
insert into letsMeetMigrationScheme.tblGenderInterest
(gender, user_id)
select  letsmeetmigrationscheme.tblgender.gender, letsmeetmigrationscheme.tblursprung.user_id  
from letsmeetmigrationscheme.tblursprung
left join letsmeetmigrationscheme.tblgender 
on (letsmeetmigrationscheme.tblursprung.interesse ILIKE '%' || letsmeetmigrationscheme.tblgender.gender || '%');
