/*CREATE DATABASE letsmeetdb;*/

/* Gemeinsamer Namensraum */
CREATE SCHEMA IF NOT EXISTS letsMeetMigrationScheme;

/* Zuerst Tabellen mit Fremdschlüsseln löschen, dann die referenzierten */
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblFriendship;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGenderInterest;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblProfilePicture;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblPicture;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUserLikesPeopleWithHobby;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby2User;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblHobby;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUser;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblGender;
DROP TABLE IF EXISTS letsMeetMigrationScheme.tblUrsprung;

CREATE TABLE letsMeetMigrationScheme.tblUrsprung (
    user_id SERIAL PRIMARY KEY,
    namen CHAR(64),      /* Vorname und Nachname nicht über 32 */
    adresse CHAR(128),   /* Ort und Straße nicht über 64 */
    telefon CHAR(16),    /* bei einer Nr. nicht über 16 */
    hobbytext CHAR(255), /* 255 better be safe than sorry... */
    email CHAR(64),      /* realistisch nicht über 64 */
    geschlecht CHAR(4),  /* m,w,nb => max 4 + Puffer */
    interesse CHAR(8),   /* m,w,nb => max 4 + Puffer */
    geburtstag CHAR(10)  /* DD.MM.YYYY => max 10 Zeichen */
);

CREATE TABLE letsMeetMigrationScheme.tblHobby (
   hobby_id SERIAL PRIMARY KEY,
   hobbyname CHAR(127) NOT NULL UNIQUE
);

CREATE TABLE letsMeetMigrationScheme.tblGender (
      gender CHAR(8) PRIMARY KEY
);

CREATE TABLE letsMeetMigrationScheme.tblUser (
    user_id SERIAL PRIMARY KEY,
    firstname CHAR(32) NOT NULL,
    lastname CHAR(32) NOT NULL,
    telephone CHAR(16),
    street CHAR(64),
	street_no CHAR(16),
    postcode CHAR(16),
    city CHAR(64),
    country CHAR(64),
    email CHAR(64),
    gender CHAR(8),
    birthday DATE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);


CREATE TABLE letsMeetMigrationScheme.tblHobby2User (
    user_id INTEGER,
    hobby_id INTEGER,
    priority INTEGER,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);

CREATE TABLE letsMeetMigrationScheme.tblUserLikesPeopleWithHobby (
    user_id INTEGER,
    hobby_id INTEGER,
    priority INTEGER,
    PRIMARY KEY (hobby_id, user_id),
    FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);


CREATE TABLE letsMeetMigrationScheme.tblGenderInterest(
    gender CHAR(8),
    user_id INTEGER,
    PRIMARY KEY (gender, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (gender) REFERENCES letsMeetMigrationScheme.tblGender(gender)
);


CREATE TABLE letsMeetMigrationScheme.tblFriendship(
    user_id INTEGER,
    friend_user_id INTEGER,
    PRIMARY KEY (friend_user_id, user_id),
    FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
    FOREIGN KEY (friend_user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE
);

CREATE TABLE letsMeetMigrationScheme.tblPicture (
  picture_id SERIAL PRIMARY KEY,
  user_id INTEGER,
  picture_url text,
  pictureData bytea,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  CHECK ((picture_url IS NOT NULL) OR (pictureData IS NOT NULL))
 );
 
CREATE TABLE letsMeetMigrationScheme.tblProfilePicture(
  user_id INTEGER PRIMARY KEY,
  picture_id INTEGER NOT NULL UNIQUE,
  FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) ON DELETE CASCADE,
  FOREIGN KEY (picture_id) REFERENCES letsMeetMigrationScheme.tblPicture(picture_id) ON DELETE CASCADE
);