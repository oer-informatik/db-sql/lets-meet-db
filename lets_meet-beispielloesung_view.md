# Erweiterung der Let's Meet-Datenbank um eine View und Trigger

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/lets_meet-beispielloesung_view</span>

> **tl/dr;** _Auswertung der vorhandenen und importierten Daten der ["Let's Meet"-Datenbank](https://oer-informatik.de/lets-meet)._

Dieser Artikel ist Teil einer mehrteiligen Serie, in der

- [die eigentliche Aufgabestellung](https://oer-informatik.de/lets-meet) das Ausgangsszenario vorstellt und den Datenbank-Export als Tabelle verlinkt,

- im [ersten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) das Zielsystem modelliert wird (ER-Modell, Relationenmodell, physisches Modell / DDL),

- im [zweiten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_import) SQL-Befehle für den Import aus der Tabellenkalkulation erstellt werden und

- im [dritten Teil der Beispiellösung](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) die Ausgangsdaten über Zeichenkettenfunktionen, Datumsfunktionen, JOINS und Vereinigung von Abfragen schließlich Daten in das neue Zielsystem übernommen werden.

- in diesem Teil wird die Datenbank nun genutzt, um SQL Übungsaufgaben damit durchzugehen.

Allen Artikel enden mit einem SQL-Skript, mit dem Daten als Ausgangspunkt für die nächste Etappe genutzt werden können, falls es zu Problemen bei der Eigenentwicklung kam.

## Ausgangslage

Die eigentliche Aufgabestellung [der "Let's Meet" Datenbank-Lernsituation kann hier](https://oer-informatik.de/lets-meet) nachgelesen werden. Voraussetzung für die in diesem Teil beschriebene Normalisierung ist eine fertig modellierte und importierte Datenbank (siehe  [Teil 1 ](https://oer-informatik.de/lets_meet-beispielloesung_modellierung) und [Teil 2](https://oer-informatik.de/lets_meet-beispielloesung_import)).  

## Schnelleinstieg ohne Vorkenntnisse: Vorbereitung des DB-Containers 

Die Grundlagen der beiden vorigen Artikel münden in einer Datenbank sie sich mit SQL-Skripten erzeugen und füllen lässt. 
Wer so weit ist, der muss nur seinen Container wieder starten (sofern dieser nicht noch läuft) und die folgenden Zeilen überspringen bis zum Absatz "Normalisierung vorbereiten in tblUrsprung".

Bei wem dies nicht geglückt ist, der kann mit folgenden Schritten einen befüllten PostgreSQL-Container erzeugen, der direkt startklar ist. Hierfür wird nur ein leeres Verzeichnis und Docker benötigt. In das Verzeichnis müssen alle SQL-Skripte mit benötigten Befehlen kopiert werden, die zuvor erstellt (oder von [hier (Struktur aus Teil 1)](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql) und [hier (Daten aus Teil 2)](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql) heruntergeladene) wurden. Die Skripte werden in alphabetischer Reihenfolge ausgeführt. In der Bash und Powershell könnte das z.B. so aussehen:

```powershell
cd ~/Documents
mkdir psqldocker
cd psqldocker
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql
wget https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql
```

Dieses SQL-Skript wird automatisch ausgeführt, wenn wir in diesem Ordner mit folgendem Befehl einen postgreSQL-Container erzeugen und starten:

```powershell
docker run --name=letsmeetpsql -d -p 3312:5432 -e POSTGRES_PASSWORD=_hier%1GutesPWnu+2en -v ${PWD}:/docker-entrypoint-initdb.d postgres:latest
```

(unter Linux muss statt `${PWD}:` mit runden Klammern `$(pwd):` geschrieben werden)

Das dauert diesmal etwas, weil 1576 Zeilen importiert werden müssen. Wenn alles geklappt hat, dann sollte im Container die interaktive (`-it`) PostgreSQL-Kommandozeile (`psql`) für Nutzer `postgres` mit Passwort `postgres` geöffnet werden können:

```powershell
docker exec -it letsmeetpsql psql -U postgres postgres
```

Die psql-Kommandozeile sieht etwa so aus:

```bash
psql (15.0 (Debian 15.0-1.pgdg110+1))
Type "help" for help.

postgres=# 
```

Und ein Befehl wie dieser hier sollte im Idealfall die Anzahl der Zeilen (1576) ausgeben:

```sql
SELECT COUNT(*) FROM letsMeetMigrationScheme.tblUrsprung
```

Mit `\q` kann die Konsole wieder verlassen werden.

Weitere Details im Umgang mit PostgreSQL im Container und mit dem Zugriff eines Frontends finden sich [hier](https://oer-informatik.de/docker-install-postgres).


## Vorbereitung der Auswertung durch Erstellung einer _View_

Erstellen Sie eine Abfrage, mit Hilfe derer Sie lets-meet-kunden identifizieren, die aufgrund ihrer Hobbies gut zusammenpassen
	
Speichern sie diese Abfrage als View in ihrer Datenbank
	
Dokumentieren Sie Abfrage und den Befehl, mit dem Sie die View erstellen in ihrem Repo.
	
Wie kann die View aufgerufen werden? 



## Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)


## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db](https://gitlab.com/oer-informatik/db-sql/lets-meet-db).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
