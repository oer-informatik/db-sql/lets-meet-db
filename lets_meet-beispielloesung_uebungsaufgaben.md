## SQL-Übungsaufgaben (zur "Let’s Meet"-Datenbank)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/lets_meet-beispielloesung_uebungsaufgaben</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110378362964551953</span>

> **tl/dr;** _Übungsaufgaben zu SQL-DDL- und DML-Befehlen am Beispiel der ["Let's Meet"-Datenbank](https://oer-informatik.de/lets-meet). Da deren Daten als SQL-Importskript vorliegen, können die Aufgaben auch unmittelbar im DBMS ausprobiert werden..._

### Ausgangslage

Ausgangsbasis der folgenden Aufgaben ist die Datenbank einer Dating/Meeting-App, die anhand des folgenden Relationenmodells erstellt wurde. Es sollen bei den Lösungen die hier abgedruckten Namen verwendet werden:

![Relationenmodell der Let's Meet Datenbank](beispielloesung/plantuml/rm_beispielloesung.png)

Die Fragen basieren auf der Datenbank aus [der "Let's Meet" Datenbank-Lernsituation](https://oer-informatik.de/lets-meet), können aber auch ohne Hintergrundinfos nur mit obigem Relationenmodell bearbeitet werden. 

Wer die Abfragen mit einer gefüllten Datenbank ausprobieren will, der kann die folgenden SQL-Skripte importieren. Es müssen alle drei SQL-Skripte der Reihe nach ausgeführt werden. 

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_1_struktur_mariadb.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_2_import_einzeiler_mariadb.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_3_normierung_mariadb.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mariadb/letsmeet_3_normierung_mariadb.sql)

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_1_struktur_mssqlserver.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_2_import_einzeiler_mssqlserver.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_3_normierung_mssqlserver.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/mssqlserver/letsmeet_3_normierung_mssqlserver.sql)

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_1_struktur_ddl_postgre.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_2_import_einzeiler_postgre.sql)

[https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_3_normierung_postgre.sql](https://gitlab.com/oer-informatik/db-sql/lets-meet-db/-/raw/main/beispielloesung/postgresql/letsmeet_3_normierung_postgre.sql)

</span>

Für Hintergrundwissen lohnt sich ein Blick in die anderen Beiträge, in denen die Datenbank [modelliert (Teil 1)](https://oer-informatik.de/lets_meet-beispielloesung_modellierung), [importiert (Teil 2)](https://oer-informatik.de/lets_meet-beispielloesung_import) und [im Bestand normalisiert (Teil 3)](https://oer-informatik.de/lets_meet-beispielloesung_normalisierung) wird.

### Aufgaben

#### Erstellen der Tabelle tblHobby2User

a) Erstelle den SQL-Befehl, mit dem die Tabelle `tblHobby2User` wie oben angegeben erzeugt werden kann. Die IDs sind in `tblHobby` und `tblUser` als fortlaufende Nummern realisiert. Die Priorität soll als Gleitkommazahl umgesetzt werden. Es muss keine Lösch- und Änderungsweitergabe berücksichtigt werden. <button onclick="toggleAnswer('db1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db1">

```sql
CREATE TABLE tblHobby2User ( /* 1P */
 user_id INTEGER, /* 0,5P */
 hobby_id INTEGER, /* 0,5P */
 priority DOUBLE, /* 1P */
 PRIMARY KEY (hobby_id, user_id), /* 1P */
 FOREIGN KEY (hobby_id) REFERENCES letsMeetMigrationScheme.tblHobby(hobby_id), /*1P*/
 FOREIGN KEY (user_id) REFERENCES letsMeetMigrationScheme.tblUser(user_id) /*1P*/
);
```

Bewertung 6 Punkte: `CREATE` 1P / Datentypen 2P / PK 2P / FK 1P

</span>

b) Welche Anpassung muss am Befehl aus Aufgabe a) vorgenommen werden, wenn das Feld `priority` eingabepflichtig sein soll? (Wenn es oben bereits eingabepflichtig umgesetzt wurde: welcher Bestandteil des SQL-Befehls ist dafür verantwortlich?) <button onclick="toggleAnswer('db2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db2">

```sql
priority DOUBLE NOT NULL, //1P
```

</span>

#### Ausgabe von Userdaten

In der Tabelle `tblUser` sind beispielhaft folgende Daten enthalten (Auszug).

|user_id|firstname|lastname|telephone|street|street_no|
|---|---|---|---|---|---|
|708|Gheorghe|Florentina|05222 / 92995|Kirchstraße|50| 
|719|Edith|Vehlow|0611 / 17692|Uhlandstr.|67|
|720|Stefan|Hubert|(030) 4108976|Am Vogelanger|21|
|736|Marcus|Göllnitz|(02702) 78293|Rembrandtstr.|37 b| 
|739|Szymański|Dariusz|06897 / 55039|Markt|27|
|746|Sabine|Hubert|07752 / 41061|Jarmener Str.|49| 

a) Nenne den SQL-Befehl, mit dem nur die Vornamen aller User ausgegeben werden, die mit Nachnamen "Hubert" heißen. Die Ausgabe soll nach der `user_id` absteigend sortiert sein. <button onclick="toggleAnswer('db3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db3">

```sql
SELECT firstname /* 1P */
FROM letsmeetmigrationscheme.tblUser /* 1P */
WHERE lastname LIKE 'Hubert' /* 2P */
ORDER BY user_id DESC /* 1P */
```

Bewertung: 5 Punkte

</span>

b) Die Telefonnummern sind auf zweierlei Art erfasst: "030 / 12345" und "(030) 12345". Erstelle einen SQL-Befehl, mit dem alle Daten von Datensätzen ausgegeben werden, deren Telefonnummern in der Schreibweise mit Schrägstrich erfasst sind! Die Ausgabe soll nach Telefonnummern aufsteigend sortiert sein. <button onclick="toggleAnswer('db4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db4">

```sql
SELECT * /* 1P */
FROM letsmeetmigrationscheme.tblUser /* 1P */
WHERE telephone LIKE '%/%'/* 2P */
ORDER BY telephone ASC; /* 2P */
```
Bewertung: 5 Punkte

</span>

#### Auswertung der Alterstruktur

a) Es soll für jede Stadt ermittelt werden, an welchem Datum der jüngste und der älteste User (m/w/nb) geboren wurde. Die Ausgabe soll wie folgt aussehen (Sortierung egal):

|Stadt|Ältester|Jüngster|
|---|---|---|
|Hamburg|1958-04-05|2002-04-24|
|Lübeck|1958-01-24|2001-09-30|
|Berlin|1959-04-14|2002-12-05|
|Kiel|1958-05-29|2001-12-26|
|Dortmund|1959-03-24|2002-08-27|
|Essen|1959-07-26|2002-08-13|

<button onclick="toggleAnswer('db5')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db5">

```sql
SELECT city AS Stadt, /* Aliasse 1P */
MIN(birthday) AS Ältester, MAX(birthday) AS Jüngster
/* Aggregationen 2x1P */
FROM letsMeetMigrationScheme.tblUser /* 1P */
GROUP BY city /*2P*/
```

</span>

b) Mithilfe der Funktion `extract(year from age(birthday))` kann beispielsweise in PostgreSQL das Alter eines Users in Jahren (als Zahl) ausgegeben werden:

```sql
SELECT extract(year from age(birthday)) AS Alter from tblUser; 
```

Erstelle mithilfe dieses Befehls eine Abfrage, die alle Städte auflistet, in denen mehr als 10 User leben, die 50 Jahre oder älter sind. Es soll die Stadt und die Anzahl der mindestens 50-jährigen wie folgt dargestellt werden, Sortierung egal: 

|Stadt|Anzahl|
|---|---|
|Berlin|24|
|Hamburg|18|

<button onclick="toggleAnswer('db6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db6">

```sql
SELECT city AS "Stadt", COUNT(*) AS "Anzahl" /* 4P */
FROM letsmeetmigrationscheme.tblUser /* 1P */
WHERE extract(year FROM age(birthday)) >=50 /* 2P */
GROUP BY city /* 1P */
HAVING COUNT(*) > 10; /* 2P */
```

Bewertung gesamt: 10 Punkte

</span>

#### Einfügen neuer Nutzer

In die Datenbank soll ein neuer Nutzer aufgenommen werden. Erstelle alle Abfragen, die nötig sind, um die angezeigten Daten zu speichern. Sofern Du nicht mit Subqueries arbeitest und zum Einfügen IDs aus anderen Tabellen benötigst, erstelle die Abfragen, mit denen Du diese IDs auslesen kannst und nenne fiktive Ergebnisse zum Weiterarbeiten. Es kann davon ausgegangen werden, dass es nur eine Userin "Martina Mustermann" und einen "Peter Meyer" gibt und dass das Hobby "Tanzen" bereits in der Datenbank unter diesem Namen angelegt ist.

|firstname|lastname|postcode|city|Hobby|Hobby Prio|befreundet mit|
|---|---|---|---|---|---|---|
|Peter |Meyer|31319|Sehnde|Tanzen|100|Martina Mustermann|

<button onclick="toggleAnswer('db7')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db7">

```sql
/*ERSTE TABELLE 3P*/
INSERT INTO tblUser /*1P*/
(user_id, firstname, lastname, postcode , city) /*1P*/
VALUES (DEFAULT, 'Peter', 'Meyer', '31319', 'Sehnde'); /*1P*/

/*9P = ZWEITE TABELLE 3P, 2x SUBQUERY 3P*/
INSERT INTO tblHobby2User /*1P*/
(user_id, hobby_id, priority) /*1P*/
VALUES (
(SELECT user_id FROM tblUser WHERE firstname LIKE 'Peter%' AND lastname LIKE 'Meyer%'), /*2P*/
(SELECT hobby_id FROM tblHobby WHERE hobbyname like 'Tanzen%'),  /*4P*/
100); /*1P*/

/*7P = DRITTE TABELLE 3P, 2x SUBQUERY 2P*/
INSERT INTOo tblFriendship/*1P*/
(user_id, friend_user_id) /*1P*/
VALUES (
(SELECT user_id FROM tblUser WHERE firstname LIKE 'Peter%' AND lastname LIKE 'Meyer%'), /*2P*/
(SELECT user_id FROM tblUser WHERE firstname LIKE 'Martina%' AND lastname LIKE 'Mustermann%') /*2P*/
) /*1P*/;
```

Bewertung 19 Punkte

</span>

#### Analyse Profilbilder und Freundschaften

a) Es sollen alle Nutzer per E-Mail angeschrieben werden, die über kein Profilbild (`tblProfilePicture`) verfügen. Hierzu soll eine Abfrage erstellt werden, die alle in der Datenbank befindlichen Adressen ausgibt, für die kein Profilbild hinterlegt ist.

<button onclick="toggleAnswer('db8')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db8">

```sql
SELECT email /*1P*/
FROM tblUser AS U /*1P*/
LEFT JOIN tblprofilepicture AS P /*1P*/
ON U.user_id = P.user_id /*1P*/
WHERE p.user_id IS NULL; /*1P*/

Alternativ
SELECT email /*1P*/
FROM tblUser /*1P*/
WHERE user_id NOT IN (/*1P*/
SELECT user_id  FROM tblprofilepicture /*2P*/
);
```

</span>

b) Ausgegeben werden sollen alle Einträge der `tblUser` und deren zugehörige Freunde (über `tblFriendship`) wie unten angegeben, Sortierung egal.

|Vorname1|Nachname1|Vorname2|Nachname2|
|---|---|---|---|
|Peter|Meyer|Martina|Mustermann|
|Peter|Meyer|Helmut|Meyerhof|
|Kay|Fabian|NULL|NULL|
|Marco|Kriener|Cetin|Hamide|
|Marco|Kriener|Sybille|Dropmann|

<button onclick="toggleAnswer('db9')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db9">

```sql
SELECT t.firstname AS Vorname1, t.lastname AS Nachname1, 
t2.firstname AS Vorname2, t2.lastname AS Nachname2/*2P*/
FROM tbluser t  /*1P*/
LEFT JOIN tblfriendship tfs  ON t.user_id = tfs.user_id  /*2P*/
LEFT JOIN tbluser t2 ON tfs.friend_user_id = t2.user_id  /*2P*/;
```

</span>

#### Anpassungen in der Datenbank

a) Der "Gerhard-Schröder-Platz" in München wurde umbenannt und heißt jetzt "Angela-Merkel-Forum". Passe die betreffenden Adressen in `tblUser` entsprechend an! 	

|<button onclick="toggleAnswer('db10')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db10">

```sql
UPDATE tbluser /*1P*/
SET street = 'Angela-Merkel-Forum' /*2P*/
WHERE street LIKE 'Gerhard-Schröder-Platz' AND city LIKE ’München’; /*2P*/
```

</span>


b) Die Priorisierung in `tblHobby2User` wurde bislang mit Zahlen von 0-100 umgesetzt. Um eine größere Bandbreite abzudecken sollen alle Werte jetzt um den Faktor 10 erhöht werden (zukünftig also zwischen 0 und 1000 sein). Erstelle eine Abfrage, um den Bestand anzupassen! 

<button onclick="toggleAnswer('db11')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="db11">

```sql
UPDATE tblhobby2user /*1P*/
SET priority = priority *10; /*3P*/
```

</span>

### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)
